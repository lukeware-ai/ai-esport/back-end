import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, BatchNormalization
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import LearningRateScheduler


# Função para carregar os dados e remover as colunas especificadas
def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de entrada a partir de um arquivo CSV."""
    # Carregar todos os dados
    data = pd.read_csv(file_path)

    # Remover as colunas especificadas
    columns_to_remove = ["tl_0", "tv_0", "tl_1", "tv_1", "rs"]
    data = data.drop(columns=columns_to_remove)

    return data


# Função para criar o gerador
def build_generator(latent_dim, output_dim):
    model = Sequential(
        [
            Dense(64, activation="relu", input_dim=latent_dim),
            BatchNormalization(),
            Dense(128, activation="relu"),
            BatchNormalization(),
            Dense(output_dim, activation="linear"),  # Usar linear para valores maiores que 1
        ]
    )
    return model


# Função para criar o discriminador
def build_discriminator(input_dim):
    model = Sequential(
        [
            Dense(128, activation="relu", input_dim=input_dim),
            Dropout(0.3),
            Dense(64, activation="relu"),
            Dropout(0.3),
            Dense(1, activation="sigmoid"),
        ]
    )
    model.compile(
        loss="binary_crossentropy",
        optimizer=Adam(learning_rate=0.0002, beta_1=0.5),
        metrics=["accuracy"],
    )
    return model


# Função para criar a GAN combinando o gerador e o discriminador
def build_gan(generator, discriminator):
    discriminator.trainable = False
    model = Sequential([generator, discriminator])
    model.compile(loss="binary_crossentropy", optimizer=Adam(learning_rate=0.0002, beta_1=0.5))
    return model


# Função para treinar a GAN
def train_gan(
    generator,
    discriminator,
    gan,
    data,
    latent_dim,
    epochs=100,
    batch_size=32,
    sample_interval=100,
):
    for epoch in range(epochs):
        # Treinar o discriminador
        idx = np.random.randint(0, data.shape[0], batch_size)
        real_data = data.iloc[idx].values  # Selecionar dados reais
        noise = np.random.randn(batch_size, latent_dim)
        fake_data = generator.predict(noise)

        # Verificar e corrigir a dimensão dos dados gerados
        fake_data = fake_data[:, : data.shape[1]]  # Ajuste para garantir que tem dimensão correta

        d_loss_real = discriminator.train_on_batch(real_data, np.ones((batch_size, 1)))
        d_loss_fake = discriminator.train_on_batch(fake_data, np.zeros((batch_size, 1)))
        d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)

        # Treinar a GAN
        noise = np.random.randn(batch_size, latent_dim)
        g_loss = gan.train_on_batch(noise, np.ones((batch_size, 1)))

        if epoch % sample_interval == 0:
            print(f"Epoch {epoch}, Discriminator Loss: {d_loss[0]}, Generator Loss: {g_loss}")


# Função para gerar amostras usando o gerador treinado
def generate_samples(generator, latent_dim, num_samples):
    noise = np.random.randn(num_samples, latent_dim)
    generated_samples = generator.predict(noise)
    return generated_samples


# Função para salvar os dados sintéticos em um arquivo CSV
def save_generated_data(generated_samples, output_file, columns):
    df = pd.DataFrame(generated_samples, columns=columns)
    df = df.round(2)  # Arredondar os valores a 2 casas decimais
    df.to_csv(output_file, index=False)
    print(f"Dados sintéticos salvos em {output_file}")


def main():
    # Configurações
    file_path = "bot-web/data/dados_gerais.csv"
    output_file = "bot-web/data/dados_gerais_sinteticos.csv"
    latent_dim = 100

    # Carregar os dados
    data = load_data(file_path)

    # Determinar output_dim com base nas colunas restantes após a remoção
    output_dim = data.shape[1]

    # Construir e compilar o gerador e o discriminador
    generator = build_generator(latent_dim, output_dim)
    discriminator = build_discriminator(output_dim)
    gan = build_gan(generator, discriminator)

    # Treinar a GAN
    train_gan(generator, discriminator, gan, data, latent_dim)

    # Gerar amostras
    num_samples = 1000  # Número de amostras a serem geradas
    generated_samples = generate_samples(generator, latent_dim, num_samples)

    # Salvar os dados sintéticos em um arquivo CSV
    save_generated_data(generated_samples, output_file, data.columns)


if __name__ == "__main__":
    main()
