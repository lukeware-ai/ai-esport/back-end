from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import Input, Dense
from tensorflow.python.keras.optimizers import adam_v2
from tensorflow.python.keras.regularizers import l2
from tensorflow import keras
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, LabelEncoder

# Carregar o arquivo CSV
df = pd.read_csv("bot-web/data/dados_gerais.csv")

# Unificar as colunas usando pd.melt
# df_melted = pd.melt(df, value_vars=['tl_0', 'tv_0'], var_name='origem',
#                     value_name='todos_os_times').drop(columns=['origem']).drop_duplicates()

# Codificar e escalar os valores
encoder = LabelEncoder()
scaler = MinMaxScaler()

# df_melted['todos_os_times_encoder'] = encoder.fit_transform(df_melted['todos_os_times'])
# df_melted['todos_os_times_scaler'] = scaler.fit_transform(df_melted[['todos_os_times_encoder']])
df["resultado_enconder"] = encoder.fit_transform(df["rs"])
df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

# Criar mapeamento e adicionar colunas escaladas ao DataFrame original
# team_to_scaler = dict(zip(df_melted['todos_os_times'], df_melted['todos_os_times_scaler']))
# df['tl_scaler'] = df['tl_0'].map(team_to_scaler)
# df['tv_scaler'] = df['tv_0'].map(team_to_scaler)

# mediana_gols_tl = df.groupby('tl_0')['gols_tl'].median().reset_index()
# mediana_gols_tl.columns = ['tl_0', 'media_gols_tl']

# mediana_gols_tv = df.groupby('tv_0')['gols_tv'].median().reset_index()
# mediana_gols_tv.columns = ['tv_0', 'media_gols_tv']

# df["media_gols_tl"] = mediana_gols_tl["media_gols_tl"]
# df["media_gols_tv"] = mediana_gols_tv["media_gols_tv"]

# df["media_gols_tl"] = df["media_gols_tl"].fillna(df['gols_tl'].median())
# df["media_gols_tv"] = df["media_gols_tv"].fillna(df['gols_tv'].median())

# selected_columns = ["media_gols_tl", "media_gols_tv", "odd_1", "odd_x", "odd_2", "media_pontos_tl", "media_pontos_tv", "posicao_camp_tl", "posicao_camp_tv", "media_probabilidade_tl", "media_probabilidade_tv", "media_formacao_tl", "media_formacao_tv", "posse_bola_tl", "posse_bola_tv", "expectativa_gol_tl", "expectativa_gol_tv", "lances_importantes_tl", "lances_importantes_tv",
#                     "finalizacoes_tl", "finalizacoes_tv", "finalizacoes_gol_tl", "finalizacoes_gol_tv", "defesa_tl", "defesa_tv", "escanteios_tl", "escanteios_tv", "faltas_tl", "faltas_tv", "passes_tl", "passes_tv", "desarmes_tl", "desarmes_tv", "cartoes_a_tl", "cartoes_a_tv", "cartoes_v_tl", "cartoes_v_tv", "tl_scaler", "tv_scaler", "resultado_scaler", "media_idade_tl", "media_idade_tv"]
# dataset = df[selected_columns]
# dataset['media_odds'] = (dataset["odd_1"] + dataset["odd_x"] + dataset["odd_2"]) / 3

# colunas_com_tl = dataset.filter(like='tl').columns
# dataset['media_geral_tl'] = dataset[colunas_com_tl].mean(axis=1)

# colunas_com_tv = dataset.filter(like='tv').columns
# dataset['media_geral_tv'] = dataset[colunas_com_tv].mean(axis=1)


print(df.head())

# Separando as features (X) e as labels (y)
X = df.drop(["resultado_scaler", "resultado_enconder", "tl_0", "tv_0", "rs"], axis=1)
y = df["resultado_scaler"]

# Dividindo o dataset em conjunto de treino e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Padronizando (normalizando) os dados
standard = StandardScaler()
X_train = standard.fit_transform(X_train)
X_test = standard.transform(X_test)

PERSENT_DROPOUT = 0.2
LEARNING_RATE = 0.001

# Criando o modelo da rede neural
entrada: Input = keras.layers.Input(
    shape=(X_train.shape[1],)
)  # Camada de entrada com a forma do número de features
# Primeira camada densa com 100 neurônios e função de ativação ReLU
camada_1: Dense = keras.layers.Dense(150, activation="relu")(entrada)
# dropout_1 = keras.layers.Dropout(PERSENT_DROPOUT)(camada_1)
camada_2: Dense = keras.layers.Dense(100, activation="relu")(camada_1)
# dropout_3 = keras.layers.Dropout(PERSENT_DROPOUT)(camada_2)
camada_3: Dense = keras.layers.Dense(50, activation="relu")(camada_2)
# dropout_4 = keras.layers.Dropout(PERSENT_DROPOUT)(camada_3)
# camada_4: Dense = keras.layers.Dense(25, activation='relu')(camada_3)
# dropout_5 = keras.layers.Dropout(PERSENT_DROPOUT)(camada_4)
# camada_5: Dense = keras.layers.Dense(10, activation='relu')(dropout_5)

# Camada de saída com número de neurônios igual ao número de classes e função de ativação softmax
saida: Dense = keras.layers.Dense(1, activation="softmax")(camada_3)

# Exibindo os dados processados
model: Model = keras.models.Model(inputs=entrada, outputs=saida)

print(model.summary())
optimizer = keras.optimizers.Adam(learning_rate=LEARNING_RATE)

model.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=["accuracy"])

history = model.fit(X_train, y_train, batch_size=4, epochs=100, verbose=1, validation_split=0.2)

acuracia = model.evaluate(X_test, y_test, verbose=1)
print(f"Acurácia: {acuracia[1]*100:.2f}%")

# Fazendo previsões
# predictions = model.predict(X_test, batch_size=4, verbose=1)

# result = X_test

# # Transformação inversa e exibição dos resultados
# result = pd.DataFrame(X_test)  # Cria um DataFrame com os dados de teste
# result["resultado_predito"] = predictions  # Adiciona os resultados preditos

# # Aplicando inversa ao scaler
# result["resultado_descaler"] = scaler.inverse_transform(result[["resultado_predito"]])

# # Aplicando inversa ao encoder
# result["resultado_deencoder"] = encoder.inverse_transform(result["resultado_descaler"].astype(int))

# print(result)
