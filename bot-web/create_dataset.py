from pathlib import Path

import pandas as pd


def main() -> None:
    out = Path("./out")
    Path("./dataset").mkdir(parents=True, exist_ok=True)

    dfs: list[pd.DataFrame] = []

    for file in out.rglob("*.csv"):
        df: pd.DataFrame = pd.read_csv(str(file))
        dfs.append(df)

    dataset_unico = pd.concat(dfs, ignore_index=True)
    dataset_unico = dataset_unico.dropna()
    dataset_unico.to_csv("./dataset/dataset_unico.csv", index=False)
    return None


if __name__ == "__main__":
    main()
