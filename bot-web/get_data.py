from datetime import datetime
from pathlib import Path
import re
from time import sleep
import unicodedata
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class GetData:
    quantidade_invalido = 0
    quantidade_campos = 71

    def __init__(self, link, output_file="output-test.csv"):
        self.link = link
        self.output_file = output_file
        self.data = {}
        self.count = 0
        self.driver = None

    def __enter__(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--window-size=1800,1200")
        options.add_argument("--headless")
        options.add_argument("--incognito")
        self.driver = webdriver.Chrome(options=options)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.driver:
            self.driver.quit()

    def run(self):
        try:
            self.driver.get(self.link)
            self._get_time()
            self._get_gols()
            self._probabilidades()
            self._posse_bola()
            self._get_odds()
            self._geral()
            self._process_data()
        except Exception as e:
            print(f"Erro exceção ocorreu durante o processamento do link: {self.link} {e}")
            with open("bot-web/erros/erros.txt", "a") as f:
                f.write(self.link + "\n")

    def _process_data(self) -> None:
        if len(self.data) != self.quantidade_campos:
            if len(self.data) > self.quantidade_campos:
                chaves = list(self.data.keys())
                for _ in range(len(self.data) - self.quantidade_campos):
                    chave = chaves.pop()
                    self.data.pop(chave)

        if len(self.data) == self.quantidade_campos:
            output_file = self.output_file.split("/")[2]
            filename = output_file.split(".")[0]
            path = "_".join(re.findall(r"\d+", self.output_file)[:2])
            file_path = Path(f"bot-web/out/{path}/{filename}.csv")
            try:

                if self.data["tl_1"] > self.data["tv_1"]:
                    self.data["rs"] = "vitoria_time_local"
                elif self.data["tl_1"] < self.data["tv_1"]:
                    self.data["rs"] = "vitoria_time_visitante"
                else:
                    self.data["rs"] = "empate"

                self.data["dt"] = "-".join(re.findall(r"\d+", self.output_file))
                df_new = pd.DataFrame([self.data])
                df_new["dt"] = pd.to_datetime(df_new["dt"]).dt.date

                file_path.parent.mkdir(parents=True, exist_ok=True)
                if not file_path.exists():
                    file_path.touch()
                    with open(file_path, "a") as f:
                        df_new.to_csv(file_path, index=False)
                    return

                df = pd.read_csv(file_path)

                # Verifica se os DataFrames têm as mesmas colunas
                if list(df_new.columns) == list(df.columns):
                    df = pd.concat([df, df_new], ignore_index=True)
                    df.to_csv(file_path, index=False)

            except Exception as e:
                GetData.quantidade_invalido = GetData.quantidade_invalido + 1
                print(f"quantidade de informções que apareceram: {GetData.quantidade_invalido}")
                raise Exception(f"Erro ocorrido: {e}")
        else:
            GetData.quantidade_invalido = GetData.quantidade_invalido + 1
            print(
                f"Quantidade de jogos que estão divergentes do padão necessários: {GetData.quantidade_invalido}"
            )
            print(f"Quantidade de Campos: {len(self.data)}")

    def _converter_texto_para_numero(self, texto):
        padrao = r"([\d.]+)\s*=\s*([\d.]+)%"
        resultado = re.match(padrao, texto)
        if resultado:
            numero = float(resultado.group(1))
            porcentagem = float(resultado.group(2)) / 100.0
            return numero, porcentagem
        else:
            raise ValueError("Erro Formato de entrada inválido")

    def _transforma_texto(self, texto):
        texto = texto.lower()
        texto = texto.replace(" ", "_")
        texto = unicodedata.normalize("NFKD", texto).encode("ASCII", "ignore").decode("ASCII")
        texto = re.sub(r"[^a-zA-Z0-9_]", "", texto)
        return texto

    def _get_time(self):
        time = []
        try:
            elementos = WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located((By.CSS_SELECTOR, ".Text.elglCn, .Text.elglCn"))
            )
            for elemento in elementos:
                time.append(self._transforma_texto(elemento.text))
        except Exception as e:
            raise Exception(f"Erro ocorrido: {e}")

        if time:
            for index in range(len(time)):
                if index % 2 == 0:
                    self.data[f"tl_{self.count}"] = time[index]
                else:
                    self.data[f"tv_{self.count}"] = time[index]

                if f"tl_{self.count}" in self.data and f"tv_{self.count}" in self.data:
                    self.count += 1

    def _get_odds(self):
        odds = []
        try:
            elementos = WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located((By.XPATH, "//span[@class='Text dNyKsG']"))
            )
            for elemento in elementos:
                odds.append(float(elemento.text))
        except Exception as e:
            raise Exception(f"Erro ocorrido: {e}")
        else:
            if odds:
                self.data["odds_1"] = odds[0]
                self.data["odds_x"] = odds[1]
                self.data["odds_2"] = odds[2]

    def _get_gols(self):
        gols = []
        try:
            elementos = WebDriverWait(self.driver, 5).until(
                EC.presence_of_all_elements_located(
                    (By.CSS_SELECTOR, ".Text.cuVfWD, .Text.hWVmjp, .Text.hzbACF")
                )
            )
            for elemento in elementos:
                gols.append(elemento.text)
        except Exception as e:
            raise Exception(f"erro ocorrido: {e}")
        else:
            if gols:
                gols = [elemento for elemento in gols if elemento != "-"]
                for index in range(len(gols)):
                    if index % 2 == 0:
                        self.data[f"tl_{self.count}"] = int(gols[index])
                    else:
                        self.data[f"tv_{self.count}"] = int(gols[index])

                    if f"tl_{self.count}" in self.data and f"tv_{self.count}" in self.data:
                        self.count += 1

    def _posse_bola(self):
        posse_bola = []
        try:
            elementos = WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located((By.XPATH, "//span[@class='Text gxbNET']"))
            )
            for elemento in elementos:
                posse_bola.append(elemento.text)
        except Exception as e:
            raise Exception(f"Erro ocorrido: {e}")
        else:
            if posse_bola:
                for index in range(len(posse_bola)):
                    if index % 2 == 0:
                        self.data[f"tl_{self.count}"] = (
                            float(posse_bola[index].replace("%", "")) / 100
                        )
                    else:
                        self.data[f"tv_{self.count}"] = (
                            float(posse_bola[index].replace("%", "")) / 100
                        )

                    if f"tl_{self.count}" in self.data and f"tv_{self.count}" in self.data:
                        self.count += 1

    def _geral(self):
        def converter_valores_percentuais(valores):
            resultados = []
            pattern = r"\((\d+)%\)"  # Expressão regular para encontrar o valor entre parênteses

            for valor in valores:
                match = re.search(pattern, valor)
                if match:
                    valor_percentual = float(match.group(1)) / 100
                    resultados.append(valor_percentual)
                else:
                    resultados.append(float(valor))

            return resultados

        geral = []
        try:
            elementos = WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located((By.CSS_SELECTOR, ".Text.iZtpCa, .Text.lfzhVF"))
            )
            for elemento in elementos:
                geral.append(elemento.text)
        except Exception as e:
            raise Exception(f"Erro ocorrido: {e}")
        else:
            geral = converter_valores_percentuais(geral)
            if geral:
                for index in range(len(geral)):
                    if index % 2 == 0:
                        self.data[f"tl_{self.count}"] = float(geral[index])
                    else:
                        self.data[f"tv_{self.count}"] = float(geral[index])

                    if f"tl_{self.count}" in self.data and f"tv_{self.count}" in self.data:
                        self.count += 1

    def _probabilidades(self):
        media_geral = []
        try:
            elementos = WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located(
                    (By.XPATH, "//div[starts-with(@class, 'Box eRaqYE')]")
                )
            )
            for elemento in elementos:
                valor_1, valor_2 = self._converter_texto_para_numero(elemento.text)
                media_geral.append((valor_1, valor_2))
        except Exception as e:
            raise Exception(f"Erro ocorrido: {e}")
        else:
            if media_geral:
                if len(media_geral) == 1:
                    self.data[f"tl_prob_a"] = media_geral[0][0]
                    self.data[f"tl_prob_b"] = media_geral[0][1]
                    self.data[f"tv_prob_a"] = 0.0
                    self.data[f"tv_prob_b"] = 0.0
                else:
                    for index in range(len(media_geral)):
                        if index % 2 == 0 and len(media_geral[index]) > 1:
                            self.data[f"tl_prob_a"] = media_geral[index][0]
                            self.data[f"tl_prob_b"] = media_geral[index][1]
                        else:
                            self.data[f"tv_prob_a"] = media_geral[index][0]
                            self.data[f"tv_prob_b"] = media_geral[index][1]


# Example usage:
if __name__ == "__main__":
    link = "https://www.sofascore.com/pt/santa-clara-b-sad/ekbslkb#id:8953898"
    with GetData(
        link, output_file="bot-web/home/www_sofascore_com_pt_futebol_2021_03_11.txt"
    ) as scraper:
        data = scraper.run()
        print(data)
