from uu import Error
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import os
from os.path import dirname, realpath, sep, isdir
import re
from urllib.parse import urlparse


class GetLinks:

    @classmethod
    def load_links(cls, link_home):
        def url_to_filename(url):
            parsed_url = urlparse(url)
            url_path = f"{parsed_url.netloc}{parsed_url.path}"
            return re.sub(r"[^a-zA-Z0-9]", "_", url_path)

        def links(driver) -> list:
            dados_novos = []
            try:
                elementos = WebDriverWait(driver, 5).until(
                    EC.presence_of_all_elements_located(
                        (
                            By.XPATH,
                            "//a[starts-with(@class, 'sc-hCPjZK iQuAyE event-hl-')]",
                        )
                    )
                )
                for elemento in elementos:
                    dados_novos.append(elemento.get_attribute("href"))

            except Exception as e:
                raise Error(f"Erro Elementos não encontrados: {e}")
            finally:
                return dados_novos

        # Configurações do WebDriver
        options = webdriver.ChromeOptions()
        options.add_argument("--window-size=1800,1200")
        options.add_argument("--headless")
        options.add_argument("--incognito")

        try:
            driver = webdriver.Chrome(options=options)
            driver.get(link_home)

            links = links(driver)

            # Criação do diretório se não existir
            folder = os.path.join(dirname(realpath(__file__)), "home")
            os.makedirs(folder, exist_ok=True)

            # Lista para armazenar os links antes de escrever no arquivo
            links_to_write = []

            for link in links:
                if "#id" in link:
                    links_to_write.append(link + "\n")

            # Abre o arquivo para adicionar novos links (modo 'a')
            # filename = str(link_home).replace("-", "_").replace("https://", "").replace(".", "_").replace("/", "_")
            filename = url_to_filename(link_home)
            file_path = os.path.join(folder, f"{filename}.txt")
            with open(file_path, "w") as f:
                f.writelines(links_to_write)

        except WebDriverException as e:
            print(f"Erro ao executar o WebDriver: {e}")

        finally:
            if "driver" in locals():
                driver.quit()


# Exemplo de uso
if __name__ == "__main__":
    GetLinks.load_links("https://www.sofascore.com/pt/futebol/2024-04-20")
