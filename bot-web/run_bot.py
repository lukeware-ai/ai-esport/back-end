import os
from time import sleep
import multiprocessing
from concurrent.futures import ThreadPoolExecutor, as_completed
from tqdm import tqdm
from get_links import GetLinks
from get_data import GetData
from clean_file import CleanFile
from pathlib import Path
from functools import partial


class RunBot:
    lock = multiprocessing.Lock()

    @staticmethod
    def run(links: list[str]) -> None:
        RunBot._load_links(links)
        RunBot._get_data()
        RunBot._clean_files()

    @staticmethod
    def _clear_console():
        os.system("cls" if os.name == "nt" else "clear")

    @staticmethod
    def _get_data():
        links = RunBot._listar_arquivos()
        for output_file in links:
            RunBot._get_data_from_files(output_file)

    @staticmethod
    def _load_links(links: list[str]) -> None:
        max_threads = 8
        with tqdm(total=len(links), desc="Carregando links") as pbar:
            with ThreadPoolExecutor(max_workers=max_threads) as executor:
                futures = [executor.submit(GetLinks.load_links, link) for link in links]

                for future in as_completed(futures):
                    try:
                        future.result()
                        pbar.update(1)
                    except Exception as e:
                        print(f"Erro exceção ocorreu durante o carregamento do link: {e}")

    @staticmethod
    def _clean_files() -> str:
        caminho = Path("bot-web/home")
        for file in caminho.iterdir():
            if file.is_file():
                try:
                    os.remove(file)
                except Exception as e:
                    print(f"Erro ao remover o arquivo '{file}': {e}")

    @staticmethod
    def _minerar() -> str:
        csv_directory = "bot-web/data"
        output_file_prefix = "novos_dados"
        CleanFile.combine_csv_files(csv_directory, output_file_prefix)

    @staticmethod
    def _listar_arquivos():
        caminho = Path("bot-web/home")
        files: list[str] = []
        for arquivo in caminho.iterdir():
            if arquivo.is_file():
                files.append(str(arquivo))
        return files

    @staticmethod
    def _get_data_from_files(output_file: str) -> None:
        try:
            with open(output_file, "r") as f:
                links = [link.strip() for link in f.readlines()]
                max_threads = 8
                process_link_com_param_adicional = partial(RunBot._process_link, output_file)

                with ThreadPoolExecutor(max_workers=max_threads) as executor:
                    with tqdm(total=len(links), desc="Processando links") as pbar:
                        futures = {
                            executor.submit(process_link_com_param_adicional, link): link
                            for link in links
                        }

                        for future in as_completed(futures):
                            try:
                                future.result()
                                pbar.update(1)
                            except Exception as e:
                                print(f"Erro exceção ocorreu durante o processamento do link: {e}")

        except FileNotFoundError:
            print(f"Arquivo '{output_file}' não encontrado.")
        except Exception as e:
            print(f"Erro ocorreu um erro ao processar o arquivo '{output_file}': {e}")

    @staticmethod
    def _process_link(output_file: str, link: str) -> None:
        try:
            with GetData(link, output_file) as scraper:
                scraper.run()
        except Exception as e:
            print(f"Erro exceção ocorreu ao obter dados do link '{link}': {e}")


if __name__ == "__main__":
    links_to_process = ["https://www.sofascore.com/pt/cobresal-sao-paulo/GOsrnb#id:12172522"]
    RunBot.run(links_to_process)
