from datetime import datetime
import os
import csv
from pathlib import Path


class CleanFile:
    @classmethod
    def limpar_registros_duplicados(cls, input_file, output_file):
        # Conjunto para armazenar links únicos
        unique_links = set()

        # Leitura dos links do arquivo
        with open(input_file, "r") as f:
            for line in f:
                line = line.strip()
                if line:
                    unique_links.add(line)

        # Escrita dos links únicos de volta para o arquivo de saída
        with open(output_file, "w") as f:
            for link in unique_links:
                f.write(link + "\n")

        # Remover o arquivo de entrada após a conclusão da limpeza
        try:
            os.remove(input_file)
        except Exception as e:
            print(f"Erro ao remover o arquivo '{input_file}': {e}")

    @classmethod
    def combine_csv_files(cls, directory: str, output_file_prefix: str):
        # Diretório onde os arquivos CSV estão localizados
        directory = Path(directory)

        # Listar todos os arquivos CSV na pasta
        csv_files = list(directory.glob("*.csv"))

        # Verificar se há arquivos CSV para processar
        if not csv_files:
            print("Nenhum arquivo CSV encontrado para processar.")
            return

        # Gerar um nome para o arquivo combinado usando timestamp
        data_atual = datetime.now()
        timestamp = int(data_atual.timestamp())
        combined_file = directory / f"{output_file_prefix}_{timestamp}_final.csv"

        # Abrir o arquivo combinado para escrita
        with open(combined_file, "w", newline="") as outfile:
            writer = None

            # Iterar sobre todos os arquivos CSV
            for csv_file in csv_files:
                with open(csv_file, "r", newline="") as infile:
                    reader = csv.reader(infile)
                    headers = next(reader)  # Ler os cabeçalhos do arquivo atual

                    # Inicializar o escritor do arquivo combinado
                    if writer is None:
                        writer = csv.writer(outfile)
                        writer.writerow(headers)  # Escrever os cabeçalhos no arquivo combinado

                    # Escrever todas as linhas do arquivo atual no arquivo combinado
                    for row in reader:
                        writer.writerow(row)

        # Deletar os arquivos CSV antigos
        # for csv_file in csv_files:
        #   os.remove(csv_file)
        #   print(f"Arquivo '{csv_file}' foi deletado.")


if __name__ == "__main__":
    input_file = "bot-web/home/ai-esports-links-categorias.txt"
    output_file = "bot-web/home/ai-esports-links-categorias-unicos.txt"

    CleanFile.limpar_registros_duplicados(input_file, output_file)
    print(f"Links únicos foram salvos em '{output_file}'.")

    # Diretório onde os arquivos CSV estão localizados
    csv_directory = "bot-web/home"
    output_file_prefix = "novos_dados"

    CleanFile.combine_csv_files(csv_directory, output_file_prefix)
