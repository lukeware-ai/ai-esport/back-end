from run_bot import RunBot

from datetime import datetime, timedelta


def main() -> None:
    data_inicial = "2021-02-01"
    # data_final = "2021-02-02"
    data_final = "2024-06-26"

    dt_inicial = datetime.strptime(data_inicial, "%Y-%m-%d").date()
    dt_final = datetime.strptime(data_final, "%Y-%m-%d").date()

    links = []
    dominio = "https://www.sofascore.com/pt/futebol"
    current_date = dt_inicial
    while current_date <= dt_final:
        links.append(f'{dominio}/{current_date.strftime("%Y-%m-%d")}')
        current_date += timedelta(days=1)

    RunBot.run(links)
    return None


if __name__ == "__main__":
    main()
