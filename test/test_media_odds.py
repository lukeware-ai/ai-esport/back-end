import unittest

import pandas as pd

from src.constants import GOLS_TL, GOLS_TV
from src.media_odds import MediaOdds


class TestMediaOdds(unittest.TestCase):

    def setUp(self) -> None:
        self.df = pd.read_csv("test/file/dados_gerais.csv")

    def test_media_odd_1(self):
        media_odd_1 = MediaOdds.media_odd_1(df=self.df, time="flamengo")
        self.assertEqual(media_odd_1[0], 1.53)

    def test_media_odd_x(self):
        media_odd_x = MediaOdds.media_odd_x(df=self.df, tl="flamengo", tv="sao_paulo")
        self.assertEqual(media_odd_x[0], 5.05375)
