import unittest

import pandas as pd


from src.analise_desenpenho import AnaliseDesenpenho
from src.supervision.gradient_boosting import GradientBoosting
from src.supervision.linear_regression import LinearRegress
from src.supervision.polynomial import PolinominalAI


class TestAnaliseDesenpenho(unittest.TestCase):

    def setUp(self) -> None:
        self.df = pd.read_csv("test/file/dados_gerais.csv")

    def test_get_model(self):
        data = {
            "tl": ["flamengo"],
            "tv": ["sao_paulo"],
            "media_pontos_tl": [7.02],
            "media_pontos_tv": [7.04],
            "posicao_camp_tl": [-1],
            "posicao_camp_tv": [-5],
        }
        df_new = pd.DataFrame(data)
        AnaliseDesenpenho.gerar_medias(df=self.df, df_new=df_new)

        # model_1, encoder_1 = AnaliseDesenpenho.model_dtree(df=self.df)
        # predict_1 = AnaliseDesenpenho.predict_1X2(df=df_new, model=model_1, encoder=encoder_1)

        # X, y, encoder = GradientBoosting.init(df=self.df)
        # model_2 = GradientBoosting.model(X=X, y=y)
        # predict_2 = AnaliseDesenpenho.predict_1X2(df=df_new, model=model_2, encoder=encoder)

        # model_3, encoder_3 = AnaliseDesenpenho.model_ridge(df=self.df)
        # predict_3 = AnaliseDesenpenho.predict_1X2(df=df_new, model=model_3, encoder=encoder_3)

        # X, y, encoder = LinearRegress.init(df=self.df)
        # model_4 = LinearRegress.model(X=X, y=y)
        # predict_4 = AnaliseDesenpenho.predict_1X2(df=df_new, model=model_4, encoder=encoder)

        # X, y, encoder = PolinominalAI.init(df=self.df)
        # model_5, encoder_5 = PolinominalAI.model(X=X, y=y, encoder=encoder)
        # predict_5 = AnaliseDesenpenho.predict_1X2(df=df_new, model=model_5, encoder=encoder_5)

        # media_tl, media_tv = AnaliseDesenpenho.media_geral(df=self.df)
        # prob_vitoria_tl, prob_empate, prob_vitoria_tv = AnaliseDesenpenho.probabilidade(df=self.df, df_new=df_new)

        data = {
            # "previsao_1x2_1": predict_1,
            "prob_ai_2": predict_2,
            # "previsao_1x2_3": predict_3,
            # "previsao_1x2_4": predict_4,
            # "previsao_1x2_5": predict_5,
            # "media_geral_tl": media_tl,
            # "media_geral_tv": media_tv,
            # "prob_vitoria_tl": prob_vitoria_tl,
            # "prob_empate": prob_empate,
            # "prob_vitoria_tv": prob_vitoria_tv,
            "resultado": "vitoria",
        }

        print(data)

        #  qualcular o 1X2 novo
        # proxima falidação vai ter

        # - predicao_1_X_2
        # - probabilidade_1
        # - probabilidade_X
        # - probabilidade_2
        # - media_geral_tv
        # - media_geral_tl
        # - resultado: (0, 1, 2)(empate, vitoria_tl, vitoria_tv)
