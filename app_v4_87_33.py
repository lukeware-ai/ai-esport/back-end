import pandas as pd
import joblib
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.metrics import accuracy_score, classification_report
from sklearn.ensemble import GradientBoostingClassifier

# Carregar os dados
df = pd.read_csv("bot-web/data/dados_gerais.csv")
df.to_parquet(
    "bot-web/data/dados_gerais.parquet",
    engine="pyarrow",
)
df = pd.read_parquet("bot-web/data/dados_gerais.parquet")
# dx = df[df.isna()]
# dx.to_csv('bot-web/data/novos_dados_2024_06_22-nan.csv', index=False)

# Pré-processamento dos dados
encoder = LabelEncoder()
scaler = StandardScaler()

df["resultado_enconder"] = encoder.fit_transform(df["rs"])
df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

X = df.drop(
    ["resultado_scaler", "resultado_enconder", "tl_0", "tv_0", "tl_1", "tv_1", "rs"],
    axis=1,
)
y = df["resultado_enconder"]

# Divisão dos dados em treino e teste
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.1, random_state=42, stratify=y
)

# Imputação dos valores ausentes
imputer = SimpleImputer(strategy="mean")
X_train_imputed = imputer.fit_transform(X_train)
X_test_imputed = imputer.transform(X_test)

# Criando o modelo GradientBoostingClassifier
# {'learning_rate': 0.01, 'max_depth': 5, 'n_estimators': 200, 'random_state': 42, 'subsample': 1.0}
# {'learning_rate': 0.2, 'max_depth': 4, 'n_estimators': 100, 'random_state': 84, 'subsample': 1.0}
# {'learning_rate': 0.2, 'max_depth': 6, 'n_estimators': 250, 'random_state': 42, 'subsample': 0.5}
# Acurácia com melhores parâmetros: 80.58% {'learning_rate': 0.1, 'max_depth': 5, 'max_features': 'log2', 'min_samples_leaf': 1,'min_samples_split': 10, 'n_estimators': 200, 'subsample': 1.0}
model = GradientBoostingClassifier(
    verbose=2,
    # learning_rate=1.0,
    # max_depth=5,
    # max_features= 'log2',
    # min_samples_leaf= 1,
    # min_samples_split= 10,
    # n_estimators=100,
    # random_state=42,
    # subsample=0.1,
    # -------------------------------------------
    # 80.91 %
    n_estimators=250,
    learning_rate=0.2,
    max_depth=6,
    random_state=42,
    subsample=0.8,
    # -------------------------------------------
    # min_samples_split=2,
    # min_samples_leaf=1,
    # max_features='',
    # loss="deviance",
    # criterion="friedman_mse"
)
model.fit(X_train_imputed, y_train)
y_pred = model.predict(X_test_imputed)
accuracy = accuracy_score(y_test, y_pred)
print(f"Acurácia: {accuracy * 100:.2f}%")
print("\nRelatório de Classificação:")

joblib.dump(model, "modelo_gradient_boosting.pkl")
joblib.dump(imputer, "imputer.pkl")
joblib.dump(encoder, "encoder.pkl")
joblib.dump(scaler, "scaler.pkl")

# # Definindo os parâmetros para a busca em grade, incluindo os hiperparâmetros desejados
# model = GradientBoostingClassifier(random_state=42)
# parameters = {
#     'n_estimators': [200,250,300],
#     'learning_rate': [0.02, 0.01, 0.03],
#     'max_depth': [6,10],
# }

# parameters = {
#     'n_estimators': [50, 100, 200, 250],
#     'learning_rate': [0.01, 0.05, 0.1],
#     'max_depth': [3, 5, 6],
#     'min_samples_split': [2, 5, 10],
#     'min_samples_leaf': [1, 2, 4],
#     'max_features': ['auto', 'sqrt', 'log2'],
#     'subsample': [0.6, 0.8, 1.0]
# }

# Realizando a busca em grade (Grid Search) para encontrar os melhores parâmetros
# grid_search = GridSearchCV(model, parameters, cv=5, n_jobs=-1, verbose=2)
# grid_search.fit(X_train_imputed, y_train)

# print("Melhores parâmetros encontrados:")
# print(grid_search.best_params_)

# # Avaliação do modelo com os melhores parâmetros encontrados
# best_model = grid_search.best_estimator_
# y_pred = best_model.predict(X_test_imputed)
# accuracy = accuracy_score(y_test, y_pred)
# print(f'Acurácia com melhores parâmetros: {accuracy * 100:.2f}%')

# # Exibir relatório de classificação
# print("\nRelatório de Classificação:")
# print(classification_report(y_test, y_pred, target_names=encoder.classes_))
