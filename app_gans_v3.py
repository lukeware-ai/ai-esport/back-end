import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense

# Carregar dados do CSV
df = pd.read_csv("bot-web/data/dados_gerais.csv")

data: pd.DataFrame = df["tv_10"]

# Normalizar os dados
scaler = MinMaxScaler()
data_normalized = scaler.fit_transform(data.values.reshape(-1, 1))


# Função para criar sequências de dados
def create_sequences(data, seq_length):
    xs, ys = [], []
    for i in range(len(data) - seq_length):
        x = data[i : i + seq_length]
        y = data[i + seq_length]
        xs.append(x)
        ys.append(y)
    return np.array(xs), np.array(ys)


# Definir o comprimento da sequência (quantos valores anteriores para prever o próximo)
seq_length = 10
X, y = create_sequences(data_normalized, seq_length)

# Dividir em conjuntos de treino e teste
split = int(0.8 * len(X))
X_train, X_test = X[:split], X[split:]
y_train, y_test = y[:split], y[split:]

# Construir modelo LSTM
model = Sequential([LSTM(64, activation="relu", input_shape=(seq_length, 1)), Dense(1)])

model.compile(optimizer="adam", loss="mse")

# Treinar o modelo
model.fit(X_train, y_train, epochs=50, batch_size=128, validation_data=(X_test, y_test))


# Gerar novos dados
def generate_new_data(model, seed_sequence, num_generate, scaler):
    generated_values = []
    current_sequence = np.array(seed_sequence)

    for i in range(num_generate):
        # Prever o próximo valor
        next_value = model.predict(current_sequence.reshape(1, seq_length, 1))

        # Adicionar o próximo valor gerado à lista
        generated_values.append(next_value[0, 0])

        # Atualizar a sequência para incluir o próximo valor e descartar o primeiro valor
        current_sequence = np.append(current_sequence[1:], next_value)

    # Desfazer a normalização para obter os valores reais
    generated_values = scaler.inverse_transform(np.array(generated_values).reshape(-1, 1))

    generated_values = np.round(generated_values, 2)

    return generated_values.flatten()


# Selecionar uma amostra aleatória dos dados como ponto de partida
start_index = np.random.randint(0, len(data_normalized) - seq_length)
seed_sequence = data_normalized[start_index : start_index + seq_length]

# Número de novos valores a gerar
num_generate = 10

# Gerar novos dados com base no modelo treinado
generated_data = generate_new_data(model, seed_sequence, num_generate, scaler)

print("Valores gerados:")
print(generated_data)
