"""
Relatório de Classificação:
                        precision    recall  f1-score   support

                empate       0.97      0.97      0.97      2914
    vitoria_time_local       0.99      0.99      0.99      5031
vitoria_time_visitante       0.99      0.99      0.99      3406

              accuracy                           0.98     11351
             macro avg       0.98      0.98      0.98     11351
          weighted avg       0.98      0.98      0.98     11351


Relatório de Classificação:
              precision    recall  f1-score   support

           0       0.76      0.76      0.76       292
           1       0.92      0.92      0.92       503
           2       0.89      0.89      0.89       341

    accuracy                           0.87      1136
   macro avg       0.86      0.86      0.86      1136
weighted avg       0.87      0.87      0.87      1136    

"""

import pandas as pd
import joblib
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.utils.class_weight import compute_class_weight
from sklearn.metrics import accuracy_score, classification_report
import tensorflow as tf
import numpy as np


def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de um arquivo CSV e salva em formato Parquet."""
    df = pd.read_csv(file_path)
    parquet_path = file_path.replace(".csv", ".parquet")
    df.to_parquet(parquet_path, engine="pyarrow")
    return pd.read_parquet(parquet_path)


def preprocess_data(df: pd.DataFrame) -> tuple:
    """Realiza o pré-processamento dos dados."""
    encoder = LabelEncoder()
    scaler = StandardScaler()

    df["resultado_enconder"] = encoder.fit_transform(df["rs"])
    df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

    X = df.drop(
        [
            "resultado_scaler",
            "resultado_enconder",
            "tl_0",
            "tv_0",
            "tl_1",
            "tv_1",
            "rs",
        ],
        axis=1,
    )
    y = df["resultado_enconder"]

    return X, y, encoder, scaler


def split_data(X: pd.DataFrame, y: pd.Series) -> tuple:
    """Divide os dados em conjuntos de treino e teste."""
    return train_test_split(X, y, test_size=0.1, random_state=42, stratify=y)


def impute_data(X_train: pd.DataFrame, X_test: pd.DataFrame) -> tuple:
    """Realiza a imputação dos valores ausentes."""
    imputer = SimpleImputer(strategy="mean")
    X_train_imputed = imputer.fit_transform(X_train)
    X_test_imputed = imputer.transform(X_test)
    return X_train_imputed, X_test_imputed, imputer


def train_model(X_train: pd.DataFrame, y_train: pd.Series) -> tf.keras.models.Sequential:
    """Treina o modelo usando uma rede neural com TensorFlow."""
    num_classes = len(y_train.unique())
    y_train_cat = tf.keras.utils.to_categorical(y_train, num_classes=num_classes)

    # Compute class weights
    class_weights = compute_class_weight(
        class_weight="balanced", classes=np.unique(y_train), y=y_train
    )
    class_weights = dict(enumerate(class_weights))

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(256, input_dim=X_train.shape[1], activation="relu"))
    model.add(
        tf.keras.layers.Dense(
            128, activation="relu", kernel_regularizer=tf.keras.regularizers.l2(0.01)
        )
    )
    model.add(
        tf.keras.layers.Dense(
            128, activation="relu", kernel_regularizer=tf.keras.regularizers.l2(0.01)
        )
    )  # nova camada
    model.add(
        tf.keras.layers.Dense(
            64, activation="elu", kernel_regularizer=tf.keras.regularizers.l2(0.01)
        )
    )
    model.add(tf.keras.layers.Dense(num_classes, activation="softmax"))

    optimizer = tf.keras.optimizers.Adam(learning_rate=0.001)  # 0.01
    model.compile(optimizer=optimizer, loss="categorical_crossentropy", metrics=["accuracy"])

    model.fit(X_train, y_train_cat, epochs=1, batch_size=1024, verbose=1)
    return model


def evaluate_model(
    model: tf.keras.models.Sequential, X_test: pd.DataFrame, y_test: pd.Series
) -> None:
    """Avalia o modelo treinado e imprime os resultados."""
    num_classes = len(y_test.unique())
    y_test_cat = tf.keras.utils.to_categorical(y_test, num_classes=num_classes)
    _, accuracy = model.evaluate(X_test, y_test_cat, verbose=1)
    y_pred = model.predict(X_test)
    y_pred_classes = y_pred.argmax(axis=-1)

    print(f"Acurácia: {accuracy * 100:.2f}%")
    print("\nRelatório de Classificação:")
    print(classification_report(y_test, y_pred_classes))


def save_model(model, imputer, encoder, scaler) -> None:
    """Salva o modelo e os pré-processadores."""
    model.save("modelo_tensorflow_v100.h5")
    joblib.dump(imputer, "imputer.pkl")
    joblib.dump(encoder, "encoder.pkl")
    joblib.dump(scaler, "scaler.pkl")


def main():
    # Carregar os dados
    df = load_data("bot-web/data/dados_gerais.csv")

    # Pré-processamento dos dados
    X, y, encoder, scaler = preprocess_data(df)

    # Divisão dos dados em treino e teste
    X_train, X_test, y_train, y_test = split_data(X, y)

    # Imputação dos valores ausentes
    # X_train_imputed, X_test_imputed, imputer = impute_data(X_train, X_test)

    # Treinamento do modelo
    model = train_model(X_train, y_train)

    # Avaliação do modelo
    evaluate_model(model, X_test, y_test)

    # Salvar o modelo e os pré-processadores
    save_model(model, None, encoder, scaler)


if __name__ == "__main__":
    main()
