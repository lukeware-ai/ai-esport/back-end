import pandas as pd
import numpy as np
import plotly.express as px
import plotly.figure_factory as ff
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler, LabelEncoder, MinMaxScaler

# Carregar os dados
df = pd.read_csv("bot-web/data/dados_gerais.csv")

# Pré-processamento dos dados
encoder = LabelEncoder()
scaler = StandardScaler()

# Codificação da variável 'rs' e padronização
df["resultado_enconder"] = encoder.fit_transform(df["rs"])
df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

# Remover colunas irrelevantes para análise
colunas_para_remover = [
    "resultado_scaler",
    "resultado_enconder",
    "tl_0",
    "tv_0",
    "tl_2",
    "tv_1",
    "rs",
]
X = df.drop(colunas_para_remover, axis=1)

# Função para capping de outliers


def cap_outliers(df, column, lower_quantile=0.05, upper_quantile=0.95):
    lower_bound = df[column].quantile(lower_quantile)
    upper_bound = df[column].quantile(upper_quantile)
    df[column] = np.where(df[column] < lower_bound, lower_bound, df[column])
    df[column] = np.where(df[column] > upper_bound, upper_bound, df[column])
    return df


# Aplicar capping em todas as colunas numéricas
for col in X.select_dtypes(include=np.number).columns:
    X = cap_outliers(X, col)

# Estatísticas descritivas
print(X.describe())

# Visualizações gráficas

# Box Plot
fig = px.box(X, title="Box Plot das Variáveis")
fig.show()

# Histogramas
fig = px.histogram(X, nbins=30, title="Histograma das Variáveis")
fig.show()

# Scatter Plot
fig = px.scatter(
    X,
    x=X.columns[0],
    y=X.columns[1],
    title="Scatter Plot entre a Primeira e a Segunda Variável",
)
fig.show()

# Heatmap de correlação
corr_matrix = X.corr()
fig = ff.create_annotated_heatmap(
    z=corr_matrix.values,
    x=list(corr_matrix.columns),
    y=list(corr_matrix.index),
    annotation_text=corr_matrix.round(2).values,
    colorscale="Viridis",
)
fig.update_layout(title="Heatmap de Correlação", width=1000, height=800)
fig.show()

# Pair Plot
fig = px.scatter_matrix(X)
fig.update_layout(title="Pair Plot das Variáveis")
fig.show()

# PCA
X_scaled = StandardScaler().fit_transform(X)
pca = PCA(n_components=2)
X_pca = pca.fit_transform(X_scaled)

fig = px.scatter(
    x=X_pca[:, 0],
    y=X_pca[:, 1],
    title="PCA - Primeiras 2 Componentes Principais",
    labels={"x": "Componente Principal 1", "y": "Componente Principal 2"},
)
fig.show()

# Normalização dos dados
scaler = MinMaxScaler()
X_normalized = scaler.fit_transform(X)

# Visualização dos dados normalizados
fig = px.box(
    pd.DataFrame(X_normalized, columns=X.columns),
    title="Box Plot das Variáveis Normalizadas",
)
fig.show()

# Transformação Logarítmica
X_log_transformed = X.copy()
for col in X.select_dtypes(include=np.number).columns:
    X_log_transformed[col] = np.log1p(X[col])

fig = px.box(X_log_transformed, title="Box Plot das Variáveis (Transformação Logarítmica)")
fig.show()
