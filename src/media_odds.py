import numpy as np
import pandas as pd
from .constants import GOLS_TL, GOLS_TV, ODD_1, ODD_2, ODD_X, TL, TV


class MediaOdds:

    @classmethod
    def media_odd_1(cls, df: pd.DataFrame, time: str) -> None:
        return [cls._odd(df, time, TL, ODD_1)]

    @classmethod
    def media_odd_x(cls, df: pd.DataFrame, tl: str, tv: str) -> None:
        odd_x_1 = cls._odd(df, tl, TL, ODD_X)
        odd_x_2 = cls._odd(df, tl, TV, ODD_X)

        odd_x_3 = cls._odd(df, tv, TL, ODD_X)
        odd_x_4 = cls._odd(df, tv, TV, ODD_X)

        media = (odd_x_1 + odd_x_2 + odd_x_3 + odd_x_4) / 4
        return [media]

    @classmethod
    def media_odd_2(cls, df: pd.DataFrame, time: str) -> None:
        odd_2 = cls._odd(df, time, TV, ODD_2)
        if odd_2 == 0.0:
            return [-1.0]
        return [odd_2]

    @classmethod
    def _odd(cls, df: pd.DataFrame, time: str, t: str, odd: str):
        df_found = df[df[t] == time]
        value = df_found[odd].mean()
        return 0.0 if np.isnan(value) else value
