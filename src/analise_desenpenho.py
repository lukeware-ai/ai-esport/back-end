from re import T
import pandas as pd
from sklearn.linear_model import LinearRegression, LogisticRegression, Ridge
from sklearn.metrics import (
    accuracy_score,
    classification_report,
    mean_squared_error,
    r2_score,
)
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures, StandardScaler, LabelEncoder
from sklearn.tree import DecisionTreeClassifier

from src.constants import (
    GOLS_TL,
    GOLS_TV,
    ODD_1,
    ODD_2,
    ODD_X,
    POSICAO_CAMP_TL,
    POSICAO_CAMP_TV,
    RESULTADO,
    TL,
    TV,
)
from src.media_geral import MediaGeral
from src.media_odds import MediaOdds
from scipy.stats import poisson


class AnaliseDesenpenho:

    @classmethod
    def gerar_medias(cls, df: pd.DataFrame, df_new: pd.DataFrame) -> None:
        tl = df_new[TL][0]
        tv = df_new[TV][0]

        for column in df.columns:
            if not column in [TL, TV, RESULTADO, POSICAO_CAMP_TL, POSICAO_CAMP_TV]:
                if TL in column:
                    df_new[column] = MediaGeral.media(
                        df=df, time=tl, column=column.replace(f"_{TL}", "")
                    )
                if TV in column:
                    df_new[column] = MediaGeral.media(
                        df=df, time=tv, column=column.replace(f"_{TV}", "")
                    )

        df_new[ODD_1] = MediaOdds.media_odd_1(df=df, time=tl)
        df_new[ODD_X] = MediaOdds.media_odd_x(df=df, tl=tl, tv=tv)
        df_new[ODD_2] = MediaOdds.media_odd_2(df=df, time=tv)

        return None

    @classmethod
    def model_ridge(cls, df: pd.DataFrame) -> tuple[Ridge, LabelEncoder]:
        df_new_copy = df.copy()

        encoder = LabelEncoder()
        df_new_copy[TL] = encoder.fit_transform(df_new_copy[TL])
        df_new_copy[TV] = encoder.fit_transform(df_new_copy[TV])

        X = df_new_copy.drop([RESULTADO, GOLS_TL, GOLS_TV], axis=1).fillna(0.0)
        y = df_new_copy[RESULTADO]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

        model = Ridge()
        param_grid = {
            "alpha": [0.1, 1.0, 10.0, 100.0],
            "solver": [
                "auto",
                "svd",
                "cholesky",
                "lsqr",
                "sparse_cg",
                "sag",
                "saga",
                "lbfgs",
            ],
        }

        grid_search = GridSearchCV(
            estimator=model, param_grid=param_grid, cv=3, n_jobs=-1, verbose=2
        )
        grid_search.fit(X_train, y_train)
        best_model = grid_search.best_estimator_
        y_pred = best_model.predict(X_test)

        print(f"Melhores hiperparâmetros: {grid_search.best_params_}")
        print(f"Mean Squared Error: {mean_squared_error(y_test, y_pred):.2f}")
        print(f"R² Score: {r2_score(y_test, y_pred):.2f}")

        return best_model, encoder

    @classmethod
    def model_dtree(cls, df: pd.DataFrame) -> tuple[DecisionTreeClassifier, LabelEncoder]:
        df_new_copy = df.copy()

        encoder = LabelEncoder()
        df_new_copy[TL] = encoder.fit_transform(df_new_copy[TL])
        df_new_copy[TV] = encoder.fit_transform(df_new_copy[TV])

        X = df_new_copy.drop([RESULTADO, GOLS_TL, GOLS_TV], axis=1).fillna(0.0)
        y = df_new_copy[RESULTADO]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

        model = DecisionTreeClassifier(random_state=42)
        param_grid = {
            "criterion": ["gini", "entropy"],
            "splitter": ["best", "random"],
            "max_depth": [None, 10, 20, 30],
            "min_samples_split": [2, 10, 20],
            "min_samples_leaf": [1, 5, 10],
            "max_features": [None, "sqrt", "log2"],
        }
        grid_search = GridSearchCV(
            estimator=model, param_grid=param_grid, cv=3, n_jobs=-1, verbose=2
        )
        grid_search.fit(X_train, y_train)

        best_model = grid_search.best_estimator_
        y_pred = best_model.predict(X_test)

        print(f"Melhores hiperparâmetros: {grid_search.best_params_}")
        print(f"Accuracy: {accuracy_score(y_test, y_pred)}")
        print(classification_report(y_test, y_pred))

        return best_model, encoder

    @classmethod
    def predict_1X2(cls, df: pd.DataFrame, model: any, encoder: LabelEncoder) -> float:
        df[TL] = encoder.fit_transform(df[TL])
        df[TV] = encoder.fit_transform(df[TV])

        X = df.drop([GOLS_TL, GOLS_TV], axis=1).fillna(0.0)
        predict = model.predict(X)
        if predict[0] != 0.0 and predict[0] != 1.0 and predict[0] != 2.0:
            return None

        return predict[0]

    @classmethod
    def media_geral(cls, df: pd.DataFrame) -> tuple[float]:
        encoder = LabelEncoder()
        df_new = df.copy()
        df_new[TL] = encoder.fit_transform(df_new[TL])
        df_new[TV] = encoder.fit_transform(df_new[TV])

        media_tl = df_new.filter(like=TL).sum(axis=1)
        media_tv = df_new.filter(like=TV).sum(axis=1)
        media_geral = media_tl + media_tv
        media_tl = ((media_tl * 100) / media_geral) / 100
        media_tv = ((media_tv * 100) / media_geral) / 100
        return (media_tl[0], media_tv[0])

    @classmethod
    def probabilidade(cls, df: pd.DataFrame, df_new: pd.DataFrame) -> tuple[float]:
        QUANTIDADE_PROBABILIDADE = df.shape[0]

        media_gols_tl = df.groupby(TL)[GOLS_TL].mean()
        media_gols_tv = df.groupby(TV)[GOLS_TV].mean()
        media_geral = df[[GOLS_TL, GOLS_TV]].mean().mean()

        tl = df_new[TL][0]
        tv = df_new[TV][0]
        media_goals_tl = (media_gols_tl[tl] + media_gols_tv[tl]) / 2
        media_goals_tv = (media_gols_tv[tv] + media_gols_tl[tv]) / 2

        expectativa_goals_tl = media_goals_tl * media_geral / media_goals_tl
        expectativa_goals_goals_tv = media_goals_tv * media_geral / media_goals_tl

        prob_gols_tl = [
            poisson.pmf(i, expectativa_goals_tl) for i in range(QUANTIDADE_PROBABILIDADE)
        ]
        prob_gols_tv = [
            poisson.pmf(i, expectativa_goals_goals_tv) for i in range(QUANTIDADE_PROBABILIDADE)
        ]

        prob_vitoria_tl = 0
        prob_empate = 0
        prob_vitoria_tv = 0

        for i in range(QUANTIDADE_PROBABILIDADE):
            for j in range(QUANTIDADE_PROBABILIDADE):
                if i > j:
                    prob_vitoria_tl += prob_gols_tl[i] * prob_gols_tv[j]
                elif i == j:
                    prob_empate += prob_gols_tl[i] * prob_gols_tv[j]
                else:
                    prob_vitoria_tv += prob_gols_tl[i] * prob_gols_tv[j]

        prob_vitoria_tl = 1 / prob_vitoria_tl
        prob_empate = 1 / prob_empate
        prob_vitoria_tv = 1 / prob_vitoria_tv

        return (
            prob_vitoria_tl.round(2),
            prob_empate.round(2),
            prob_vitoria_tv.round(2),
        )
