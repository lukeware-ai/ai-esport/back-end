from re import T
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV, train_test_split

from sklearn.preprocessing import StandardScaler, LabelEncoder


from src.constants import GOLS_TL, GOLS_TV, RESULTADO, TL, TV


class GradientBoosting:

    @classmethod
    def init(cls, df: pd.DataFrame):
        df_new_copy = df.copy()

        encoder = LabelEncoder()
        df_new_copy[TL] = encoder.fit_transform(df_new_copy[TL])
        df_new_copy[TV] = encoder.fit_transform(df_new_copy[TV])

        X = df_new_copy.drop([RESULTADO, GOLS_TL, GOLS_TV], axis=1).fillna(0.0)
        y = df_new_copy[RESULTADO]
        return X, y, encoder

    @classmethod
    def model(cls, X: any, y: any) -> GradientBoostingClassifier:

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

        model = GradientBoostingClassifier(random_state=42)
        param_grid = {
            "n_estimators": [100, 200, 300],
            "learning_rate": [0.01, 0.05, 0.1],
            "max_depth": [3, 4, 5],
            "min_samples_split": [2, 5, 10],
            "min_samples_leaf": [1, 2, 4],
            "subsample": [0.8, 0.9, 1.0],
        }

        grid_search = GridSearchCV(
            estimator=model, param_grid=param_grid, cv=3, n_jobs=-1, verbose=2
        )
        grid_search.fit(X_train, y_train)

        best_model = grid_search.best_estimator_
        y_pred = best_model.predict(X_test)

        print(f"Melhores hiperparâmetros: {grid_search.best_params_}")
        print(f"Accuracy: {accuracy_score(y_test, y_pred)}")

        return best_model
