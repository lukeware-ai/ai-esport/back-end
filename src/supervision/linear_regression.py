from re import T
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from src.constants import GOLS_TL, GOLS_TV, RESULTADO, TL, TV
import plotly.express as px
import plotly.graph_objs as go


class LinearRegress:

    @classmethod
    def init(cls, df: pd.DataFrame) -> tuple:
        df_new_copy = df.copy()

        encoder = LabelEncoder()
        df_new_copy[TL] = encoder.fit_transform(df_new_copy[TL])
        df_new_copy[TV] = encoder.fit_transform(df_new_copy[TV])

        X = df_new_copy.drop([RESULTADO, GOLS_TL, GOLS_TV], axis=1).fillna(0.0)
        y = df_new_copy[RESULTADO]

        return (X, y, encoder)

    @classmethod
    def model(cls, X, y) -> LinearRegression:

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

        model = LinearRegression()

        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)

        print(f"Mean Squared Error: {mean_squared_error(y_test, y_pred):.2f}")
        print(f"R² Score: {r2_score(y_test, y_pred):.2f}")

        return model
