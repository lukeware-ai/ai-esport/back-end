from re import T
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures, StandardScaler, LabelEncoder

from src.constants import GOLS_TL, GOLS_TV, RESULTADO, TL, TV
from src.media_geral import MediaGeral
from src.media_odds import MediaOdds
from scipy.stats import poisson


class PolinominalAI:

    @classmethod
    def init(cls, df: pd.DataFrame):
        df_new_copy = df.copy()

        encoder = LabelEncoder()
        df_new_copy[TL] = encoder.fit_transform(df_new_copy[TL])
        df_new_copy[TV] = encoder.fit_transform(df_new_copy[TV])

        X = df_new_copy.drop([RESULTADO, GOLS_TL, GOLS_TV], axis=1).fillna(0.0)
        y = df_new_copy[RESULTADO]
        return (X, y, encoder)

    @classmethod
    def model(cls, X: any, y: any, encoder: LabelEncoder) -> tuple[Pipeline, LabelEncoder]:

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

        pipe = Pipeline(
            steps=[
                ("scaler", scaler),
                ("poly", PolynomialFeatures(degree=2, interaction_only=False)),
                ("logistic_regression", LogisticRegression(random_state=42)),
            ]
        )

        param_grid = {
            "poly__degree": [2, 3],
            "logistic_regression__C": [0.01, 0.1, 1, 10],
            "logistic_regression__solver": ["lbfgs", "liblinear"],
        }

        grid_search = GridSearchCV(
            estimator=pipe, param_grid=param_grid, cv=3, n_jobs=-1, verbose=2
        )
        grid_search.fit(X_train, y_train)

        best_model = grid_search.best_estimator_
        y_pred = best_model.predict(X_test)

        print(f"Melhores hiperparâmetros: {grid_search.best_params_}")
        print(f"Accuracy: {accuracy_score(y_test, y_pred)}")
        print(classification_report(y_test, y_pred))

        return best_model, encoder
