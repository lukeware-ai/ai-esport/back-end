import numpy as np
import pandas as pd

from src.constants import TL, TV


class MediaGeral:

    @classmethod
    def media_em_casa(cls, df: pd.DataFrame, time: str, t: str, column: str) -> list[float]:
        return [cls._media(df, time, t, column)]

    @classmethod
    def media_fora_de_casa(cls, df: pd.DataFrame, time: str, t: str, column: str) -> list[float]:
        return [cls._media(df, time, t, column)]

    @classmethod
    def media(cls, df: pd.DataFrame, time: str, column: str) -> list[float]:
        df_new = pd.DataFrame()
        df_new["media_em_casa"] = cls.media_em_casa(df=df, time=time, t=TL, column=f"{column}_{TL}")
        df_new["media_fora_de_casa"] = cls.media_fora_de_casa(
            df=df, time=time, t=TV, column=f"{column}_{TV}"
        )
        return (df_new["media_em_casa"] + df_new["media_fora_de_casa"]) / 2

    @classmethod
    def _media(cls, df: pd.DataFrame, time: str, t: str, med_prob: str):
        df_found = df[df[t] == time]
        if not df_found.empty:
            value = df_found[med_prob].mean()
            return 0.0 if np.isnan(value) else value
        return 0.0
