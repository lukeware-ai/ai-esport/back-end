import pandas as pd
import joblib

# Carregar os dados de entrada (novos dados)
# df_novos_dados: pd.DataFrame = pd.read_csv('bot-web/data/novos_dados_2024_06_23.csv')
df_novos_dados: pd.DataFrame = pd.read_csv(
    "bot-web/out/www_sofascore_com_pt_futebol_2024_06_22.csv"
)

# modelo = joblib.load('modelo_gradient_boosting_85_47.pkl')
modelo = joblib.load("modelo_gradient_boosting_80_04.pkl")
# Carregar o encoder
encoder = joblib.load("encoder.pkl")
# Carregar o scaler (se necessário)
scaler = joblib.load("scaler.pkl")
imputer = joblib.load("imputer.pkl")

# Pré-processar os novos dados
df_novos_dados["resultado_enconder"] = encoder.transform(
    df_novos_dados["rs"]
)  # Aplica o encoder aos novos dados

# Se você usou o scaler durante o treinamento, também é necessário aplicá-lo novos dados
df_novos_dados["resultado_scaler"] = scaler.transform(df_novos_dados[["resultado_enconder"]])

# Remover colunas irrelevantes (se houver)
X_novos = df_novos_dados.drop(
    ["resultado_scaler", "resultado_enconder", "tl_0", "tv_0", "tl_1", "tv_1", "rs"],
    axis=1,
)

# Imputar valores ausentes (se houver)
X_novos_imputed = imputer.transform(X_novos)

# Fazer previsões com o modelo carregado
y_pred_novos = modelo.predict(X_novos_imputed)

# Se necessário, pode-se também calcular probabilidades com predict_proba
# y_proba_novos = modelo.predict_proba(X_novos_imputed)

# Exemplo de como usar as previsões
print("Previsões para os novos dados:")
print(y_pred_novos)

# Salvar previsões em um arquivo CSV, se desejado
df_novos_dados["previsoes"] = y_pred_novos
print(df_novos_dados)

corretas = (df_novos_dados["resultado_enconder"] == df_novos_dados["previsoes"]).sum()
incorretas = (df_novos_dados["resultado_enconder"] != df_novos_dados["previsoes"]).sum()
acuracia = (corretas * 100) / df_novos_dados["resultado_enconder"].shape[0]

print(f"\n\nCorretas: {corretas}")
print(f"Incorretos: {incorretas}")
print(f"Acurácia: {acuracia:.2f}%\n\n")
df_novos_dados.to_csv("previsoes_novos_dados.csv", index=False)
