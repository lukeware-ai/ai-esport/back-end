import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import Adam

# Carregar dados do arquivo CSV
data = pd.read_csv("dados_gerais.csv")

# Pré-processamento e normalização dos dados
numeric_cols = [
    "gols_tl",
    "gols_tv",
    "odd_1",
    "odd_x",
    "odd_2",
    "media_pontos_tl",
    "media_pontos_tv",
    "posicao_camp_tl",
    "posicao_camp_tv",
    "media_probabilidade_tl",
    "media_probabilidade_tv",
    "media_formacao_tl",
    "media_formacao_tv",
    "posse_bola_tl",
    "posse_bola_tv",
    "expectativa_gol_tl",
    "expectativa_gol_tv",
    "lances_importantes_tl",
    "lances_importantes_tv",
    "finalizacoes_tl",
    "finalizacoes_tv",
    "finalizacoes_gol_tl",
    "finalizacoes_gol_tv",
    "defesa_tl",
    "defesa_tv",
    "escanteios_tl",
    "escanteios_tv",
    "faltas_tl",
    "faltas_tv",
    "passes_tl",
    "passes_tv",
    "desarmes_tl",
    "desarmes_tv",
    "cartoes_a_tl",
    "cartoes_a_tv",
    "cartoes_v_tl",
    "cartoes_v_tv",
]

categorical_cols = ["tl", "tv"]

# Normalizar os dados numéricos
scaler = MinMaxScaler()
data[numeric_cols] = scaler.fit_transform(data[numeric_cols])

# Codificar os dados categóricos (one-hot encoding)
encoder = OneHotEncoder()
encoded_data = encoder.fit_transform(data[categorical_cols]).toarray()

# Obter os nomes das colunas para one-hot encoding
encoded_columns = []
for i, category in enumerate(encoder.categories_):
    for value in category:
        encoded_columns.append(f"{categorical_cols[i]}_{value}")

# Criar DataFrame com os dados codificados
encoded_df = pd.DataFrame(encoded_data, columns=encoded_columns)

# Concatenar os dados codificados com os dados numéricos normalizados
normalized_data = pd.concat([encoded_df, data.drop(columns=categorical_cols)], axis=1)

# Converter dados para numpy array
data_array = normalized_data.values
input_dim = data_array.shape[1]

# Função para criar o Gerador


def build_generator(input_dim):
    generator = Sequential()
    generator.add(Dense(128, input_dim=input_dim, activation="relu"))
    generator.add(Dense(256, activation="relu"))
    generator.add(Dense(input_dim, activation="tanh"))  # Saída deve ter o mesmo número de features
    return generator


# Função para criar o Discriminador


def build_discriminator(input_dim):
    discriminator = Sequential()
    discriminator.add(Dense(256, input_dim=input_dim, activation="relu"))
    discriminator.add(Dropout(0.3))
    discriminator.add(Dense(128, activation="relu"))
    discriminator.add(Dense(1, activation="sigmoid"))

    optimizer = Adam(learning_rate=0.0002, beta_1=0.5)
    discriminator.compile(loss="binary_crossentropy", optimizer=optimizer)
    return discriminator


# Função para compilar a GAN combinando Gerador e Discriminador


def build_gan(generator, discriminator):
    discriminator.trainable = False  # Garante que apenas o Gerador seja treinado
    gan = Sequential()
    gan.add(generator)
    gan.add(discriminator)
    gan.compile(
        loss="binary_crossentropy", optimizer=optimizer
    )  # Use o mesmo otimizador do discriminador
    return gan


# Inicializa Gerador e Discriminador
generator = build_generator(input_dim)
discriminator = build_discriminator(input_dim)

# Inicializa GAN
optimizer = Adam(learning_rate=0.0002, beta_1=0.5)
gan = build_gan(generator, discriminator)

# Treinamento da GAN
epochs = 10
batch_size = 40

for epoch in range(epochs):
    # Gera dados falsos
    noise = np.random.normal(0, 1, (batch_size, input_dim))
    generated_data = generator.predict(noise)

    # Seleciona dados reais aleatórios
    idx = np.random.randint(0, len(data_array), batch_size)
    real_data_batch = data_array[idx]

    # Treina o Discriminador
    d_loss_real = discriminator.train_on_batch(real_data_batch, np.ones(batch_size))
    d_loss_fake = discriminator.train_on_batch(generated_data, np.zeros(batch_size))
    d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)

    # Treina o Gerador (através da GAN)
    noise = np.random.normal(0, 1, (batch_size, input_dim))
    g_loss = gan.train_on_batch(noise, np.ones(batch_size))

    # A cada 1000 épocas, exibe o progresso
    if epoch % 1000 == 0:
        print(f"Epoch {epoch}, Discriminator Loss: {d_loss}, Generator Loss: {g_loss}")

# Após o treinamento, utilize o Gerador para gerar dados falsos
num_samples = 100
fake_data = generator.predict(np.random.normal(0, 1, (num_samples, input_dim)))
# Transforma os dados gerados de volta para DataFrame
fake_df = pd.DataFrame(fake_data, columns=normalized_data.columns)

# Inverte a normalização dos dados numéricos
fake_df[numeric_cols] = scaler.inverse_transform(fake_df[numeric_cols])

# Ajusta os valores negativos em gols_tl e gols_tv para zero
fake_df["gols_tl"] = fake_df["gols_tl"].clip(lower=0).astype(int)
fake_df["gols_tv"] = fake_df["gols_tv"].clip(lower=0).astype(int)

# Decodifica os dados categóricos
fake_categorical = encoder.inverse_transform(fake_df[encoded_columns].values)
fake_categorical_df = pd.DataFrame(fake_categorical, columns=categorical_cols)

# Remove as colunas categóricas codificadas e concatena os dados decodificados
fake_df.drop(columns=encoded_columns, inplace=True)
fake_df = pd.concat([fake_categorical_df, fake_df], axis=1)

# Salva os dados gerados como CSV
fake_df.to_csv("dados_gerados_fake.csv", index=False)
