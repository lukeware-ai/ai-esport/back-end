# Importando as bibliotecas necessárias
from numpy import shape
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow import keras
from tensorflow.python.keras.layers import Input, Dense
from tensorflow.python.keras.models import Model
import seaborn as sns

# Carregando o dataset 'iris' do seaborn
dataset = sns.load_dataset("iris")
print(dataset.head())

# Separando as features (X) e as labels (y)
X = dataset.drop(["species"], axis=1)
y = pd.get_dummies(dataset.species, prefix="output").astype(int)

# Criando um dicionário para mapear índices de volta para espécies
species_mapping = {i: species for i, species in enumerate(dataset["species"].unique())}

# Dividindo o dataset em conjunto de treino e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Padronizando (normalizando) os dados
standard = StandardScaler()
X_train = standard.fit_transform(X_train)
X_test = standard.transform(X_test)

# Criando o modelo da rede neural
entrada: Input = keras.layers.Input(
    shape=(X_train.shape[1],)
)  # Camada de entrada com a forma do número de features
# Primeira camada densa com 100 neurônios e função de ativação ReLU
camada_1: Dense = keras.layers.Dense(150, activation="relu")(entrada)
# Segunda camada densa com 50 neurônios e função de ativação ReLU
camada_2: Dense = keras.layers.Dense(100, activation="relu")(camada_1)
camada_3: Dense = keras.layers.Dense(50, activation="relu")(camada_1)
camada_4: Dense = keras.layers.Dense(25, activation="relu")(camada_1)
# Terceira camada densa com 25 neurônios e função de ativação ReLU
camada_5: Dense = keras.layers.Dense(10, activation="relu")(camada_2)

# Camada de saída com número de neurônios igual ao número de classes e função de ativação softmax
saida: Dense = keras.layers.Dense(y_train.shape[1], activation="softmax")(camada_5)

# Exibindo os dados processados
model: Model = keras.models.Model(inputs=entrada, outputs=saida)

print(model.summary())

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["acc"])

history = model.fit(X_train, y_train, batch_size=4, epochs=20, verbose=1, validation_split=0.20)

acuracia = model.evaluate(X_test, y_test, verbose=1)
print(f"Acurácia: {acuracia[1]*100:.2f}%")

# Fazendo previsões
predict = model.predict(X_test, batch_size=4, verbose=1)

# Convertendo as previsões para índices
predicted_indices = predict.argmax(axis=1)

# Mapeando os índices para as espécies
predicted_species = [species_mapping[i] for i in predicted_indices]

# Exibindo a primeira previsão
print(f"Primeira previsão: {predicted_species[0]}")

# Exibindo todas as previsões
for i, species in enumerate(predicted_species):
    print(f"Amostra {i+1}: {species}")
