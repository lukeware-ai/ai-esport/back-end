# Importando as bibliotecas necessárias
from numpy import shape
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow import keras
import seaborn as sns
from tensorflow.python.keras.models import Sequential
import seaborn as sns

# Carregando o dataset 'iris' do seaborn
dataset = sns.load_dataset("iris")
print(dataset.head())

# Separando as features (X) e as labels (y)
X = dataset.drop(["species"], axis=1)
y = pd.get_dummies(dataset.species, prefix="output").astype(int)

# Criando um dicionário para mapear índices de volta para espécies
species_mapping = {i: species for i, species in enumerate(dataset["species"].unique())}

# Dividindo o dataset em conjunto de treino e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Padronizando (normalizando) os dados
standard = StandardScaler()
X_train = standard.fit_transform(X_train)
X_test = standard.transform(X_test)

# Convertendo y_train e y_test para numpy arrays
y_train = y_train.values
y_test = y_test.values

# Criando o modelo
model: Sequential = keras.models.Sequential()
model.add(keras.layers.Dense(128, activation="relu", input_dim=X_train.shape[1]))
model.add(keras.layers.Dropout(0.20))
model.add(keras.layers.Dense(32, activation="relu"))
model.add(keras.layers.Dropout(0.20))
model.add(keras.layers.Dense(32, activation="relu"))
model.add(keras.layers.Dense(y_train.shape[1], activation="softmax"))
print(model.summary())

# Compilando o modelo
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

# Treinando o modelo
history = model.fit(X_train, y_train, batch_size=4, epochs=20, verbose=1, validation_split=0.20)

# Avaliando o modelo
acuracia = model.evaluate(X_test, y_test, verbose=1)
print(f"Acurácia: {acuracia[1]*100:.2f}%")

# Fazendo previsões
predict = model.predict(X_test, batch_size=4, verbose=1)

# Convertendo as previsões para índices
predicted_indices = predict.argmax(axis=1)

# Mapeando os índices para as espécies
predicted_species = [species_mapping[i] for i in predicted_indices]

# Exibindo a primeira previsão
print(f"Primeira previsão: {predicted_species[0]}")

# Exibindo todas as previsões
for i, species in enumerate(predicted_species):
    print(f"Amostra {i+1}: {species}")
