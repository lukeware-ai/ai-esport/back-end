# Importando as bibliotecas necessárias
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow import keras
from tensorflow.python.keras.layers import Input, Dense, SimpleRNN
from tensorflow.python.keras.models import Model
import seaborn as sns

# Carregando o dataset 'iris' do seaborn
dataset = sns.load_dataset("iris")
print(dataset.head())

# Separando as features (X) e as labels (y)
X = dataset.drop(["species"], axis=1)
y = pd.get_dummies(dataset.species, prefix="output").astype(int)

# Criando um dicionário para mapear índices de volta para espécies
species_mapping = {i: species for i, species in enumerate(dataset["species"].unique())}

# Dividindo o dataset em conjunto de treino e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Padronizando (normalizando) os dados
standard = StandardScaler()
X_train = standard.fit_transform(X_train)
X_test = standard.transform(X_test)

# Ajustando as dimensões dos dados para a entrada da RNN
# Para uma RNN, precisamos de uma sequência de dados (batch_size, timesteps, features)
# No nosso caso, assumiremos que cada linha do dataset é um timestep.

X_train_rnn = X_train.reshape(X_train.shape[0], 1, X_train.shape[1])
X_test_rnn = X_test.reshape(X_test.shape[0], 1, X_test.shape[1])

# Criando o modelo da rede neural RNN
entrada: Input = keras.layers.Input(shape=(1, X_train.shape[1]))  # Camada de entrada para a RNN
rnn_layer: SimpleRNN = keras.layers.SimpleRNN(100, activation="relu")(
    entrada
)  # Camada RNN com 100 neurônios

# Camada densa para processamento final
saida: Dense = keras.layers.Dense(y_train.shape[1], activation="softmax")(rnn_layer)

# Exibindo os dados processados
model: Model = keras.models.Model(inputs=entrada, outputs=saida)
print(model.summary())

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["acc"])

history = model.fit(X_train_rnn, y_train, batch_size=4, epochs=20, verbose=1, validation_split=0.20)

acuracia = model.evaluate(X_test_rnn, y_test, verbose=1)
print(f"Acurácia: {acuracia[1]*100:.2f}%")

# Fazendo previsões
predict = model.predict(X_test_rnn, batch_size=4, verbose=1)

# Convertendo as previsões para índices
predicted_indices = predict.argmax(axis=1)

# Mapeando os índices para as espécies
predicted_species = [species_mapping[i] for i in predicted_indices]

# Exibindo a primeira previsão
print(f"Primeira previsão: {predicted_species[0]}")

# Exibindo todas as previsões
for i, species in enumerate(predicted_species):
    print(f"Amostra {i+1}: {species}")
