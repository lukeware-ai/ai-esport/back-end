# Importando as bibliotecas necessárias
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow import keras
from tensorflow.python.keras.models import Sequential

import seaborn as sns

# Carregando o dataset 'iris' do seaborn
dataset = sns.load_dataset("iris")
print(dataset.head())

# Separando as features (X) e as labels (y)
X = dataset.drop(["species"], axis=1)
y = pd.get_dummies(dataset.species)

# Criando um dicionário para mapear índices de volta para espécies
species_mapping = {i: species for i, species in enumerate(dataset["species"].unique())}

# Dividindo o dataset em conjunto de treino e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Padronizando (normalizando) os dados
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Reshape dos dados para LSTM (amostras, timesteps, features)
X_train = X_train.reshape((X_train.shape[0], 1, X_train.shape[1]))
X_test = X_test.reshape((X_test.shape[0], 1, X_test.shape[1]))

# Criando o modelo LSTM
model: Sequential = keras.models.Sequential()
model.add(keras.layers.LSTM(100, input_shape=(X_train.shape[1], X_train.shape[2])))
model.add(keras.layers.Dense(y_train.shape[1], activation="softmax"))

# Exibindo os dados processados
print(model.summary())

# Compilando o modelo
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

# Treinando o modelo
history = model.fit(X_train, y_train, epochs=20, batch_size=4, verbose=1, validation_split=0.2)

# Avaliando o modelo
_, accuracy = model.evaluate(X_test, y_test, verbose=1)
print(f"Acurácia: {accuracy*100:.2f}%")

# Fazendo previsões
predictions = model.predict(X_test)

# Convertendo as previsões para índices
predicted_indices = predictions.argmax(axis=1)

# Mapeando os índices para as espécies
predicted_species = [species_mapping[i] for i in predicted_indices]

# Exibindo a primeira previsão
print(f"Primeira previsão: {predicted_species[0]}")

# Exibindo todas as previsões
for i, species in enumerate(predicted_species):
    print(f"Amostra {i+1}: {species}")
