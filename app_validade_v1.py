import plotly.express as px
import pandas as pd
import numpy as np

# Carregar seu dataset
# Substitua 'data.csv' pelo caminho do seu arquivo CSV
df = pd.read_csv("bot-web/data/dados_gerais.csv")
df = df.drop(columns=["tl_0", "tv_0", "rs", "tl_1", "tv_1"])

# Calcular matriz de correlação
corr_matrix = df.corr()

# Gerar o heatmap com Plotly
fig = px.imshow(
    corr_matrix,
    labels=dict(color="Correlação"),
    x=corr_matrix.columns,
    y=corr_matrix.columns,
    title="Matriz de Correlação entre Variáveis",
)

fig.show()


# Exemplo de gráfico de dispersão usando Plotly
fig = px.scatter(
    df,
    x="tl_12",
    y="tv_12",
    title="Gráfico de Dispersão: tl_12 vs. tv_12",
    labels={"tl_12": "tl_12", "tv_12": "tv_12"},
)

fig.show()


# Exemplo de gráfico de barras usando Plotly
rs_counts = df["tl_2"].value_counts()

fig = px.bar(
    x=rs_counts.index,
    y=rs_counts.values,
    title="Contagem de Resultados (tl_2)",
    labels={"x": "Resultado", "y": "Contagem"},
)

fig.show()


# Exemplo de gráfico de barras usando Plotly
rs_counts = df["tv_10"].value_counts()

fig = px.bar(
    x=rs_counts.index,
    y=rs_counts.values,
    title="Contagem de Resultados (tl_2)",
    labels={"x": "Resultado", "y": "Contagem"},
)

fig.show()
