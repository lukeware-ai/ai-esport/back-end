import pandas as pd
import joblib
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.metrics import accuracy_score, classification_report
from sklearn.ensemble import GradientBoostingClassifier


def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de um arquivo CSV e salva em formato Parquet."""
    df = pd.read_csv(file_path)
    parquet_path = file_path.replace(".csv", ".parquet")
    df.to_parquet(parquet_path, engine="pyarrow")
    return pd.read_parquet(parquet_path)


def preprocess_data(df: pd.DataFrame) -> tuple:
    """Realiza o pré-processamento dos dados."""
    encoder = LabelEncoder()
    scaler = StandardScaler()

    df["resultado_enconder"] = encoder.fit_transform(df["rs"])
    df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

    X = df.drop(
        [
            "resultado_scaler",
            "resultado_enconder",
            "tl_0",
            "tv_0",
            "tl_1",
            "tv_1",
            "rs",
        ],
        axis=1,
    )
    y = df["resultado_enconder"]

    return X, y, encoder, scaler


def split_data(X: pd.DataFrame, y: pd.Series) -> tuple:
    """Divide os dados em conjuntos de treino e teste."""
    return train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)


def impute_data(X_train: pd.DataFrame, X_test: pd.DataFrame) -> tuple:
    """Realiza a imputação dos valores ausentes."""
    imputer = SimpleImputer(strategy="mean")
    X_train_imputed = imputer.fit_transform(X_train)
    X_test_imputed = imputer.transform(X_test)
    return X_train_imputed, X_test_imputed, imputer


def train_model(X_train: pd.DataFrame, y_train: pd.Series) -> GradientBoostingClassifier:
    """Treina o modelo GradientBoostingClassifier com busca em grade."""
    param_grid = {
        "n_estimators": [64, 128, 256],
        "learning_rate": [0.1, 0.2, 0.3],
        "max_depth": [4, 6, 8],
        "subsample": [0.1, 0.5, 0.8],
    }

    grid_search = GridSearchCV(
        estimator=GradientBoostingClassifier(random_state=42),
        param_grid=param_grid,
        scoring="accuracy",
        cv=5,
        verbose=2,
        n_jobs=-1,
    )

    # Executa a busca em grade com os dados de treinamento
    grid_search.fit(X_train, y_train)

    # Após a busca em grade, imprime os melhores parâmetros encontrados
    print("Melhores parâmetros encontrados:")
    print(grid_search.best_params_)

    # Obtém o melhor modelo encontrado pela busca em grade e o treina nos dados completos
    best_model = grid_search.best_estimator_
    best_model.fit(X_train, y_train)

    return best_model


def evaluate_model(
    model: GradientBoostingClassifier, X_test: pd.DataFrame, y_test: pd.Series
) -> None:
    """Avalia o modelo treinado e imprime os resultados."""
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print(f"Acurácia: {accuracy * 100:.2f}%")
    print("\nRelatório de Classificação:")
    print(classification_report(y_test, y_pred))


def save_model(model, imputer, encoder, scaler) -> None:
    """Salva o modelo e os pré-processadores."""
    joblib.dump(model, "modelo_gradient_boosting_v7.pkl")
    joblib.dump(imputer, "imputer.pkl")
    joblib.dump(encoder, "encoder.pkl")
    joblib.dump(scaler, "scaler.pkl")


def main():
    # Carregar os dados
    df = load_data("bot-web/data/dados_gerais.csv")

    # Pré-processamento dos dados
    X, y, encoder, scaler = preprocess_data(df)

    # Divisão dos dados em treino e teste
    X_train, X_test, y_train, y_test = split_data(X, y)

    # Imputação dos valores ausentes
    X_train_imputed, X_test_imputed, imputer = impute_data(X_train, X_test)

    # Treinamento do modelo
    model = train_model(X_train_imputed, y_train)

    # Avaliação do modelo
    evaluate_model(model, X_test_imputed, y_test)

    # Salvar o modelo e os pré-processadores
    save_model(model, imputer, encoder, scaler)


if __name__ == "__main__":
    main()
