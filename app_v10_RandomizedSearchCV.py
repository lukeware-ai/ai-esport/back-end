import pandas as pd
import joblib
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import randint, uniform


def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de um arquivo CSV e salva em formato Parquet."""
    df = pd.read_csv(file_path)
    parquet_path = file_path.replace(".csv", ".parquet")
    df.to_parquet(parquet_path, engine="pyarrow")
    return pd.read_parquet(parquet_path)


def preprocess_data(df: pd.DataFrame) -> tuple:
    """Realiza o pré-processamento dos dados."""
    encoder = LabelEncoder()
    scaler = StandardScaler()

    df["resultado_enconder"] = encoder.fit_transform(df["rs"])
    df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

    X = df.drop(
        [
            "resultado_scaler",
            "resultado_enconder",
            "tl_0",
            "tv_0",
            "tl_1",
            "tv_1",
            "rs",
        ],
        axis=1,
    )
    y = df["resultado_enconder"]

    return X, y, encoder, scaler


def split_data(X: pd.DataFrame, y: pd.Series) -> tuple:
    """Divide os dados em conjuntos de treino e teste."""
    return train_test_split(X, y, test_size=0.1, random_state=42, stratify=y)


def impute_data(X_train: pd.DataFrame, X_test: pd.DataFrame) -> tuple:
    """Realiza a imputação dos valores ausentes."""
    imputer = SimpleImputer(strategy="mean")
    X_train_imputed = imputer.fit_transform(X_train)
    X_test_imputed = imputer.transform(X_test)
    return X_train_imputed, X_test_imputed, imputer


def train_model(X_train: pd.DataFrame, y_train: pd.Series) -> MLPRegressor:
    """Treina o modelo MLPRegressor."""
    # model = MLPRegressor(
    #     hidden_layer_sizes=(100, 50),  # Exemplo de configuração de camadas ocultas
    #     activation='relu',
    #     solver='adam',
    #     random_state=42
    # )
    # model.fit(X_train, y_train)
    # return model
    param_dist = {
        "hidden_layer_sizes": [(50,), (100,), (100, 50), (200, 100)],
        "activation": ["relu", "tanh"],
        "solver": ["adam", "sgd"],
        "alpha": uniform(0.0001, 0.01),
    }

    # Criar o objeto RandomizedSearchCV
    random_search = RandomizedSearchCV(
        estimator=MLPRegressor(random_state=42),
        param_distributions=param_dist,
        n_iter=50,  # Número de combinações a serem testadas
        cv=5,
        scoring="neg_mean_squared_error",
        verbose=2,
        n_jobs=-1,
    )
    # Executar a busca aleatória
    random_search.fit(X_train, y_train)

    # Melhor combinação de parâmetros
    print("Melhores parâmetros encontrados:")
    print(random_search.best_params_)
    # Avaliar o desempenho do melhor modelo
    best_model = random_search.best_estimator_

    # evaluate_model(best_model, X_test, y_test)
    return best_model


def evaluate_model(model: MLPRegressor, X_test: pd.DataFrame, y_test: pd.Series) -> None:
    """Avalia o modelo treinado e imprime os resultados."""
    y_pred = model.predict(X_test)
    mse = mean_squared_error(y_test, y_pred)
    r2 = r2_score(y_test, y_pred)
    print(f"MSE: {mse:.2f}")
    print(f"R2 Score: {r2:.2f}")


def save_model(model, imputer, encoder, scaler) -> None:
    """Salva o modelo e os pré-processadores."""
    joblib.dump(model, "modelo_mlp_regressor.pkl")
    joblib.dump(imputer, "imputer.pkl")
    joblib.dump(encoder, "encoder.pkl")
    joblib.dump(scaler, "scaler.pkl")


def main():
    # Carregar os dados
    df = load_data("bot-web/data/dados_gerais.csv")

    # Pré-processamento dos dados
    X, y, encoder, scaler = preprocess_data(df)

    # Divisão dos dados em treino e teste
    X_train, X_test, y_train, y_test = split_data(X, y)

    # Imputação dos valores ausentes
    X_train_imputed, X_test_imputed, imputer = impute_data(X_train, X_test)

    # Treinamento do modelo
    model = train_model(X_train_imputed, y_train)

    # Avaliação do modelo
    evaluate_model(model, X_test_imputed, y_test)

    # Salvar o modelo e os pré-processadores
    save_model(model, imputer, encoder, scaler)


if __name__ == "__main__":
    main()
