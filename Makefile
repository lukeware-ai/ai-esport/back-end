.PHONY: test clean

test:
	PYTHONPATH=./src python -m unittest discover -s test -p '*test_*.py'

clean:
	find . -type d -name '__pycache__' -exec rm -rf {} +

format:
	black --line-length=100 --target-version=py312 .