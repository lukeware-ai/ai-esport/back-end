import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Dropout
from tensorflow.keras.utils import to_categorical
import seaborn as sns
import matplotlib.pyplot as plt

# Carregar os dados
df = pd.read_csv("bot-web/data/novos_dados_2024_06_22.csv")

# Pré-processamento dos dados
encoder = LabelEncoder()
scaler = StandardScaler()

df["resultado_enconder"] = encoder.fit_transform(df["rs"])
df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

# Remover colunas irrelevantes
X = df.drop(["resultado_scaler", "resultado_enconder", "tl_0", "tv_0", "rs"], axis=1)
y = df["resultado_enconder"]

# Divisão dos dados em treino e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Imputação dos valores ausentes
imputer = SimpleImputer(strategy="mean")
X_train_imputed = imputer.fit_transform(X_train)
X_test_imputed = imputer.transform(X_test)

# Verifique o número de classes
num_classes = len(np.unique(y))

# Convertendo y para formato categórico
y_train = to_categorical(y_train, num_classes)
y_test = to_categorical(y_test, num_classes)

# Reshape dos dados para LSTM (amostras, timesteps, features)
X_train = X_train_imputed.reshape((X_train_imputed.shape[0], 1, X_train_imputed.shape[1]))
X_test = X_test_imputed.reshape((X_test_imputed.shape[0], 1, X_test_imputed.shape[1]))

# Criando o modelo LSTM
model = Sequential()
model.add(LSTM(128, input_shape=(X_train.shape[1], X_train.shape[2]), return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(64))
model.add(Dropout(0.2))
model.add(Dense(num_classes, activation="softmax"))

# Exibindo os dados processados
print(model.summary())

# Compilando o modelo
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

# Treinando o modelo
history = model.fit(X_train, y_train, epochs=50, batch_size=32, verbose=1, validation_split=0.2)

# Avaliando o modelo
_, accuracy = model.evaluate(X_test, y_test, verbose=1)
print(f"Acurácia: {accuracy*100:.2f}%")

# Fazendo previsões
predictions = model.predict(X_test)

# Convertendo as previsões para índices
predicted_indices = predictions.argmax(axis=1)

# Visualizando o desempenho do modelo
plt.plot(history.history["accuracy"])
plt.plot(history.history["val_accuracy"])
plt.title("Acurácia do modelo")
plt.ylabel("Acurácia")
plt.xlabel("Época")
plt.legend(["Treinamento", "Validação"], loc="upper left")
plt.show()

plt.plot(history.history["loss"])
plt.plot(history.history["val_loss"])
plt.title("Perda do modelo")
plt.ylabel("Perda")
plt.xlabel("Época")
plt.legend(["Treinamento", "Validação"], loc="upper left")
plt.show()
