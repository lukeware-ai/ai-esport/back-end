import pandas as pd
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.neural_network import MLPClassifier
from sklearn.impute import SimpleImputer
from sklearn.metrics import accuracy_score, classification_report

# Carregar os dados
df = pd.read_csv("bot-web/data/dados_gerais.csv")

# Pré-processamento dos dados
encoder = LabelEncoder()
scaler = StandardScaler()

df["resultado_enconder"] = encoder.fit_transform(df["rs"])
df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

X = df.drop(
    ["resultado_scaler", "resultado_enconder", "tl_0", "tv_0", "tl_1", "tv_1", "rs"],
    axis=1,
)
y = df["resultado_enconder"]

# Divisão dos dados em treino e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Imputação dos valores ausentes
imputer = SimpleImputer(strategy="mean")
X_train_imputed = imputer.fit_transform(X_train)
X_test_imputed = imputer.transform(X_test)

# Avaliação da acurácia no conjunto de teste
# Criando o modelo MLPClassifier
model = MLPClassifier(
    activation="logistic",
    alpha=0.0001,
    hidden_layer_sizes=(100,),
    learning_rate="constant",
    max_iter=1000,
    random_state=42,
)
model.fit(X_train_imputed, y_train)
y_pred = model.predict(X_test_imputed)
accuracy = accuracy_score(y_test, y_pred)
print(f"Acurácia: {accuracy * 100:.2f}%")

# Definindo os parâmetros para a busca em grade, incluindo os hiperparâmetros desejados
# parameters = {
#     'hidden_layer_sizes': [(100,), (100, 50), (150, 100, 50)],
#     'activation': ['relu', 'tanh', 'logistic'],
#     'alpha': [0.0001, 0.001, 0.01],
#     'learning_rate': ['constant', 'invscaling', 'adaptive'],
#     'max_iter': [1000],  # Definindo max_iter como 1000
#     'random_state': [42]  # Definindo random_state como 42
# }

# Realizando a busca em grade (Grid Search) para encontrar os melhores parâmetros
# grid_search = GridSearchCV(model, parameters, cv=5, n_jobs=-1)
# grid_search.fit(X_train_imputed, y_train)

# print("Melhores parâmetros encontrados:")
# print(grid_search.best_params_)

# Avaliação do modelo com os melhores parâmetros encontrados
# best_model = grid_search.best_estimator_
# y_pred = best_model.predict(X_test_imputed)
# accuracy = accuracy_score(y_test, y_pred)
# print(f'Acurácia com melhores parâmetros: {accuracy * 100:.2f}%')

# Exibir relatório de classificação
# print("\nRelatório de Classificação:")
# print(classification_report(y_test, y_pred, target_names=encoder.classes_))

# Outras métricas de desempenho podem ser avaliadas conforme necessário
