import statsmodels.api as sm
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

# Carregando os dados
df = pd.read_csv("bot-web/data/dados_gerais.csv")

# Preparando os dados
df = df.drop(columns=["tl_0", "tv_0", "rs", "tl_1", "tv_1"])
X = df["tl_11"]
y = df["tv_11"]

# Plotando os dados
plt.figure(figsize=(10, 5))
plt.plot(X, y, "o", label="dados não lineares")
plt.legend()
plt.xlabel("x")
plt.ylabel("y")
plt.grid()
plt.show()

X, y = np.array(X).reshape(-1, 1), np.array(y).reshape(-1, 1)


def polynominal_features(X, d=2):
    X = np.hstack([X**i for i in range(1, d + 1)])
    return X


d = 2
X = polynominal_features(X, d=d)

# Dividindo os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Treinando o modelo
reg = LinearRegression()
reg.fit(X_train, y_train)

# Fazendo previsões nos conjuntos de treinamento e teste
y_pred_train = reg.predict(X_train)
y_pred_test = reg.predict(X_test)

# Calculando o erro quadrático médio (MSE)
mse_train = mean_squared_error(y_train, y_pred_train)
mse_test = mean_squared_error(y_test, y_pred_test)

print(f"MSE train: {mse_train:.2f}")
print(f"MSE test: {mse_test:.2f}")

# Plotando os MSEs
mse_values = [mse_train, mse_test]
labels = ["Treinamento", "Teste"]

plt.bar(labels, mse_values, color=["blue", "orange"])
plt.ylabel("Erro Quadrático Médio (MSE)")
plt.title("Comparação do MSE entre Treinamento e Teste")
plt.show()

# Plotando os dados e as previsões
plt.figure(figsize=(10, 5))
plt.plot(X_train[:, 0], y_train, "o", label="Dados de treinamento")
plt.plot(X_test[:, 0], y_test, "o", label="Dados de teste")
plt.plot(X_test[:, 0], y_pred_test, "r-", label=f"Modelo grau {d}")
plt.legend()
plt.xlabel("x")
plt.ylabel("y")
plt.grid()
plt.show()
