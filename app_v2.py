import pandas as pd
from sklearn.preprocessing import MinMaxScaler, StandardScaler, LabelEncoder
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, Dropout
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import Accuracy
import numpy as np

# Função para pré-processamento dos dados


def preprocess_data(df):
    # Unificar as colunas usando pd.melt
    df_melted = (
        pd.melt(df, value_vars=["tl", "tv"], var_name="origem", value_name="todos_os_times")
        .drop(columns=["origem"])
        .drop_duplicates()
    )

    # Codificar e escalar os valores
    encoder = LabelEncoder()
    scaler = MinMaxScaler()

    df_melted["todos_os_times_encoder"] = encoder.fit_transform(df_melted["todos_os_times"])
    df_melted["todos_os_times_scaler"] = scaler.fit_transform(df_melted[["todos_os_times_encoder"]])
    df["resultado_enconder"] = encoder.fit_transform(df["resultado"])
    df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

    # Criar mapeamento e adicionar colunas escaladas ao DataFrame original
    team_to_scaler = dict(zip(df_melted["todos_os_times"], df_melted["todos_os_times_scaler"]))
    df["tl_scaler"] = df["tl"].map(team_to_scaler)
    df["tv_scaler"] = df["tv"].map(team_to_scaler)

    # Médias dos gols e preenchimento dos NaNs
    mediana_gols_tl = df.groupby("tl")["gols_tl"].median().reset_index()
    mediana_gols_tl.columns = ["tl", "media_gols_tl"]
    mediana_gols_tv = df.groupby("tv")["gols_tv"].median().reset_index()
    mediana_gols_tv.columns = ["tv", "media_gols_tv"]

    df["media_gols_tl"] = mediana_gols_tl["media_gols_tl"]
    df["media_gols_tv"] = mediana_gols_tv["media_gols_tv"]

    df["media_gols_tl"] = df["media_gols_tl"].fillna(df["gols_tl"].median())
    df["media_gols_tv"] = df["media_gols_tv"].fillna(df["gols_tv"].median())

    selected_columns = [
        "media_gols_tl",
        "media_gols_tv",
        "odd_1",
        "odd_x",
        "odd_2",
        "media_pontos_tl",
        "media_pontos_tv",
        "posicao_camp_tl",
        "posicao_camp_tv",
        "media_probabilidade_tl",
        "media_probabilidade_tv",
        "media_formacao_tl",
        "media_formacao_tv",
        "posse_bola_tl",
        "posse_bola_tv",
        "expectativa_gol_tl",
        "expectativa_gol_tv",
        "lances_importantes_tl",
        "lances_importantes_tv",
        "finalizacoes_tl",
        "finalizacoes_tv",
        "finalizacoes_gol_tl",
        "finalizacoes_gol_tv",
        "defesa_tl",
        "defesa_tv",
        "escanteios_tl",
        "escanteios_tv",
        "faltas_tl",
        "faltas_tv",
        "passes_tl",
        "passes_tv",
        "desarmes_tl",
        "desarmes_tv",
        "cartoes_a_tl",
        "cartoes_a_tv",
        "cartoes_v_tl",
        "cartoes_v_tv",
        "tl_scaler",
        "tv_scaler",
        "resultado_scaler",
        "media_idade_tl",
        "media_idade_tv",
    ]
    dataset = df[selected_columns]

    # Cálculo de média e normalização das odds
    dataset["media_odds"] = (dataset["odd_1"] + dataset["odd_x"] + dataset["odd_2"]) / 3

    colunas_com_tl = dataset.filter(like="tl").columns
    dataset["media_geral_tl"] = dataset[colunas_com_tl].mean(axis=1)

    colunas_com_tv = dataset.filter(like="tv").columns
    dataset["media_geral_tv"] = dataset[colunas_com_tv].mean(axis=1)

    return dataset, encoder, scaler


# Carregar o arquivo CSV
df = pd.read_csv("dados_gerais_fake.csv")
# df = pd.read_csv('dados_gerais_1804.csv')

# Pré-processamento dos dados
dataset, encoder, minmax_scaler = preprocess_data(df)

# Separando as features (X) e as labels (y)
X = dataset.drop(["resultado_scaler"], axis=1)
y = dataset["resultado_scaler"]

# Dividindo o dataset em conjunto de treino e teste
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=42, stratify=y
)

# Padronizando (normalizando) os dados
standard_scaler = StandardScaler()
X_train = standard_scaler.fit_transform(X_train)
X_test = standard_scaler.transform(X_test)

# Função para criação do modelo


def create_model(input_shape):
    PERSENT_DROPOUT = 0.2
    entrada = Input(shape=(input_shape,))
    camada_1 = Dense(150, activation="relu")(entrada)
    dropout_1 = Dropout(PERSENT_DROPOUT)(camada_1)
    camada_2 = Dense(100, activation="relu")(dropout_1)
    dropout_2 = Dropout(PERSENT_DROPOUT)(camada_2)
    camada_3 = Dense(50, activation="relu")(dropout_2)
    dropout_3 = Dropout(PERSENT_DROPOUT)(camada_3)
    camada_4 = Dense(25, activation="relu")(dropout_3)
    dropout_4 = Dropout(PERSENT_DROPOUT)(camada_4)
    camada_5 = Dense(10, activation="relu")(dropout_4)
    saida = Dense(1, activation="sigmoid")(camada_5)  # Usando sigmoid para classificação binária

    model = Model(inputs=entrada, outputs=saida)
    return model


# Criando o modelo
model = create_model(X_train.shape[1])

# Compilando o modelo
optimizer = Adam(learning_rate=0.001)
model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=["accuracy"])

# Sumário do modelo
print(model.summary())

# Treinamento do modelo
history = model.fit(X_train, y_train, batch_size=32, epochs=100, verbose=1, validation_split=0.2)

# Avaliação do modelo
_, accuracy = model.evaluate(X_test, y_test, verbose=1)
print(f"Acurácia: {accuracy*100:.2f}%")
model.save("modelo_treinado.h5")

# Fazendo previsões
predictions = model.predict(X_test, batch_size=4, verbose=1)

result = pd.DataFrame(X_test)  # Cria um DataFrame com os dados de teste
result["resultado_predito"] = predictions  # Adiciona os resultados preditos

# Aplicando inversa ao scaler
result["resultado_descaler"] = minmax_scaler.inverse_transform(result[["resultado_predito"]])

# Aplicando inversa ao encoder
result["resultado_deencoder"] = encoder.inverse_transform(result["resultado_descaler"].astype(int))


print(result)
