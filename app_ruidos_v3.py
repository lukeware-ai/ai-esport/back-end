import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor


def detect_anomalies_zscore(df: pd.DataFrame, threshold: float = 3.0) -> pd.DataFrame:
    """Detecta anomalias em um DataFrame usando o Z-Score."""
    z_scores = np.abs((df - df.mean()) / df.std())
    anomalies = df[(z_scores > threshold).any(axis=1)]
    return anomalies


def detect_anomalies_iqr(df: pd.DataFrame) -> pd.DataFrame:
    """Detecta anomalias em um DataFrame usando o IQR."""
    Q1 = df.quantile(0.25)
    Q3 = df.quantile(0.75)
    IQR = Q3 - Q1
    anomalies = df[((df < (Q1 - 1.5 * IQR)) | (df > (Q3 + 1.5 * IQR))).any(axis=1)]
    return anomalies


def detect_anomalies_isolation_forest(df: pd.DataFrame) -> pd.DataFrame:
    """Detecta anomalias em um DataFrame usando Isolation Forest."""
    model = IsolationForest(contamination=0.01, random_state=42)
    model.fit(df)
    df["anomaly"] = model.predict(df)
    anomalies = df[df["anomaly"] == -1]
    return anomalies


def detect_anomalies_lof(df: pd.DataFrame) -> pd.DataFrame:
    """Detecta anomalias em um DataFrame usando Local Outlier Factor."""
    model = LocalOutlierFactor(n_neighbors=20, contamination=0.01)
    df["anomaly"] = model.fit_predict(df)
    anomalies = df[df["anomaly"] == -1]
    return anomalies


# Exemplo de uso
df = pd.read_csv("bot-web/data/dados_gerais.csv")


encoder = LabelEncoder()
scaler = StandardScaler()

df["resultado_enconder"] = encoder.fit_transform(df["rs"])
df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

df = df.drop(
    ["resultado_scaler", "resultado_enconder", "tl_0", "tv_0", "tl_1", "tv_1", "rs"],
    axis=1,
)

anomalies = detect_anomalies_zscore(df)
print(anomalies)

anomalies = detect_anomalies_iqr(df)
print(anomalies)

anomalies = detect_anomalies_isolation_forest(df)
print(anomalies)

anomalies = detect_anomalies_lof(df)
print(anomalies)
