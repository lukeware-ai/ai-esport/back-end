import pandas as pd
import joblib
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.utils.class_weight import compute_class_weight
from sklearn.metrics import accuracy_score, classification_report
import tensorflow as tf
import numpy as np
import plotly.graph_objects as go
import plotly.io as pio


def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de um arquivo CSV e salva em formato Parquet."""
    df = pd.read_csv(file_path)
    parquet_path = file_path.replace(".csv", ".parquet")
    df.to_parquet(parquet_path, engine="pyarrow")
    return pd.read_parquet(parquet_path)


def preprocess_data(df: pd.DataFrame) -> tuple:
    """Realiza o pré-processamento dos dados."""
    encoder = LabelEncoder()
    scaler = StandardScaler()

    df["resultado_enconder"] = encoder.fit_transform(df["rs"])
    df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

    X = df.drop(
        [
            "resultado_scaler",
            "resultado_enconder",
            "tl_0",
            "tv_0",
            "tl_1",
            "tv_1",
            "rs",
        ],
        axis=1,
    )
    y = df["resultado_enconder"]

    return X, y, encoder, scaler


def split_data(X: pd.DataFrame, y: pd.Series) -> tuple:
    """Divide os dados em conjuntos de treino e teste."""
    return train_test_split(X, y, test_size=0.2, random_state=42, stratify=y)


def impute_data(X_train: pd.DataFrame, X_test: pd.DataFrame) -> tuple:
    """Realiza a imputação dos valores ausentes."""
    imputer = SimpleImputer(strategy="mean")
    X_train_imputed = imputer.fit_transform(X_train)
    X_test_imputed = imputer.transform(X_test)
    return X_train_imputed, X_test_imputed, imputer


def train_model(X_train: pd.DataFrame, y_train: pd.Series) -> tf.keras.models.Sequential:
    """Treina o modelo usando uma rede neural com TensorFlow."""
    num_classes = len(y_train.unique())
    y_train_cat = tf.keras.utils.to_categorical(y_train, num_classes=num_classes)

    # Compute class weights
    class_weights = compute_class_weight(
        class_weight="balanced", classes=np.unique(y_train), y=y_train
    )
    class_weights = dict(enumerate(class_weights))

    model = tf.keras.models.Sequential()
    # model.add(tf.keras.layers.LSTM(64, input_shape=(X_train.shape[1], X_train.shape[2])))
    # model.add(tf.keras.layers.Dense(512, input_dim=X_train.shape[1], activation='relu'))
    # model.add(tf.keras.layers.LeakyReLU())

    model.add(
        tf.keras.layers.Dense(512, input_dim=X_train.shape[1], activation="relu", name="layer4")
    )
    model.add(tf.keras.layers.LeakyReLU())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Dense(256, activation="relu", name="layer3"))
    model.add(tf.keras.layers.LeakyReLU())
    model.add(tf.keras.layers.Dropout(0.1))

    model.add(tf.keras.layers.Dense(128, activation="relu", name="layer2"))
    model.add(tf.keras.layers.LeakyReLU())
    model.add(tf.keras.layers.Dropout(0.1))

    # model.add(tf.keras.layers.Dense(64, activation='relu', name="layer1"))
    # model.add(tf.keras.layers.LeakyReLU())
    # model.add(tf.keras.layers.Dropout(0.1))

    # model.add(tf.keras.layers.Dense(32, activation='relu',name="layer4"))
    # model.add(tf.keras.layers.LeakyReLU())
    # model.add(tf.keras.layers.Dropout(0.1))

    # model.add(tf.keras.layers.Dense(16, activation='relu'))
    # model.add(tf.keras.layers.LeakyReLU())
    # model.add(tf.keras.layers.Dropout(0.1))

    # model.add(tf.keras.layers.Dense(8, activation='relu'))
    # model.add(tf.keras.layers.LeakyReLU())
    # model.add(tf.keras.layers.Dropout(0.1))

    # model.add(tf.keras.layers.Dense(4, activation='relu'))
    # model.add(tf.keras.layers.LeakyReLU())

    model.add(tf.keras.layers.Dense(num_classes, activation="softmax"))

    # optimizer = tf.keras.optimizers.Adam(learning_rate=0.0001)
    optimizer = tf.keras.optimizers.Adamax(learning_rate=0.001, weight_decay=0.004)
    model.compile(
        optimizer=optimizer,
        loss="categorical_crossentropy",
        metrics=[tf.keras.metrics.TopKCategoricalAccuracy(k=2)],
    )

    history = model.fit(X_train, y_train_cat, epochs=2048, batch_size=1024, verbose=1)
    # history = model.fit(X_train, y_train_cat, epochs=10, batch_size=512, verbose=1)

    # Extrair métricas do histórico de treinamento
    train_loss = history.history["loss"]
    top_k_categorical_accuracy = history.history["top_k_categorical_accuracy"]
    # train_accuracy = history.history['accuracy']
    # train_categorical_accuracy = history.history['categorical_accuracy']
    # train_recall= history.history['recall']
    # train_precision= history.history['precision']

    # # Plotar as métricas
    epochs = list(range(1, len(train_loss) + 1))
    plot_metrics_2(epochs, train_loss, top_k_categorical_accuracy)

    return model


def plot_metrics_2(epochs, train_loss, train_accuracy):
    """Plota as métricas de treinamento usando Plotly."""
    # Criar figuras com Plotly
    fig = go.Figure()

    # Adicionar curvas para as métricas
    fig.add_trace(go.Scatter(x=epochs, y=train_loss, mode="lines", name="Loss"))
    fig.add_trace(go.Scatter(x=epochs, y=train_accuracy, mode="lines", name="Accuracy"))

    # Atualizar layout do gráfico
    fig.update_layout(
        title="Métricas de Treinamento",
        xaxis_title="Épocas",
        yaxis_title="Valor",
        template="plotly_dark",  # Escolha um tema de plotagem
        legend=dict(orientation="h", yanchor="top", xanchor="center", y=-0.2, x=0.5),
    )

    # Salvar o gráfico como HTML
    pio.write_html(fig, file="training_metrics.html", auto_open=True)


def plot_metrics(
    epochs,
    train_loss,
    train_accuracy,
    train_categorical_accuracy,
    train_recall,
    train_precision,
):
    """Plota as métricas de treinamento usando Plotly."""
    # Criar figuras com Plotly
    fig = go.Figure()

    # Adicionar curvas para as métricas
    fig.add_trace(go.Scatter(x=epochs, y=train_accuracy, mode="lines+markers", name="Accuracy"))
    fig.add_trace(go.Scatter(x=epochs, y=train_loss, mode="lines", name="Loss"))
    fig.add_trace(
        go.Scatter(
            x=epochs,
            y=train_categorical_accuracy,
            mode="lines",
            name="Categorical Accuracy",
        )
    )
    fig.add_trace(go.Scatter(x=epochs, y=train_recall, mode="lines", name="Recall"))
    fig.add_trace(go.Scatter(x=epochs, y=train_precision, mode="lines", name="Precision"))

    # Atualizar layout do gráfico
    fig.update_layout(
        title="Métricas de Treinamento",
        xaxis_title="Épocas",
        yaxis_title="Valor",
        template="plotly_dark",  # Escolha um tema de plotagem
        legend=dict(orientation="h", yanchor="top", xanchor="center", y=-0.2, x=0.5),
    )

    # Salvar o gráfico como HTML
    pio.write_html(fig, file="training_metrics.html", auto_open=True)


def evaluate_model(
    model: tf.keras.models.Sequential, X_test: pd.DataFrame, y_test: pd.Series
) -> None:
    """Avalia o modelo treinado e imprime os resultados."""
    num_classes = len(y_test.unique())
    y_test_cat = tf.keras.utils.to_categorical(y_test, num_classes=num_classes)
    y_pred = model.predict(X_test)
    y_pred_classes = y_pred.argmax(axis=-1)

    # _,_,_, accuracy, categorical_accuracy = model.evaluate(X_test, y_test_cat, verbose=1)
    # print(f"\naccuracy: {accuracy * 100:.2f}%")
    # print(f"\ncategorical_accuracy: {categorical_accuracy * 100:.2f}%")
    print("\nRelatório de Classificação:")
    print(classification_report(y_test, y_pred_classes))


def save_model(model, imputer, encoder, scaler) -> None:
    """Salva o modelo e os pré-processadores."""
    model.save("modelo_tensorflow_v100.h5")
    joblib.dump(imputer, "imputer.pkl")
    joblib.dump(encoder, "encoder.pkl")
    joblib.dump(scaler, "scaler.pkl")


def main():
    # Carregar os dados
    df = load_data("bot-web/data/dados_gerais.csv")

    # Pré-processamento dos dados
    X, y, encoder, scaler = preprocess_data(df)

    # Divisão dos dados em treino e teste
    X_train, X_test, y_train, y_test = split_data(X, y)

    # Imputação dos valores ausentes
    X_train_imputed, X_test_imputed, imputer = impute_data(X_train, X_test)

    # Treinamento do modelo
    model = train_model(X_train_imputed, y_train)

    # Avaliação do modelo
    evaluate_model(model, X_test_imputed, y_test)

    # Salvar o modelo e os pré-processadores
    save_model(model, imputer, encoder, scaler)


if __name__ == "__main__":
    main()
