import pandas as pd
import joblib
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.metrics import accuracy_score, classification_report
from sklearn.ensemble import RandomForestClassifier
from skopt import BayesSearchCV
from skopt.space import Real, Categorical, Integer


def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de um arquivo CSV e salva em formato Parquet."""
    df = pd.read_csv(file_path)
    parquet_path = file_path.replace(".csv", ".parquet")
    df.to_parquet(parquet_path, engine="pyarrow")
    return pd.read_parquet(parquet_path)


def preprocess_data(df: pd.DataFrame) -> tuple:
    """Realiza o pré-processamento dos dados."""
    encoder = LabelEncoder()
    scaler = StandardScaler()

    df["resultado_enconder"] = encoder.fit_transform(df["rs"])
    df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

    X = df.drop(
        [
            "resultado_scaler",
            "resultado_enconder",
            "tl_0",
            "tv_0",
            "tl_1",
            "tv_1",
            "rs",
        ],
        axis=1,
    )
    y = df["resultado_enconder"]

    return X, y, encoder, scaler


def split_data(X: pd.DataFrame, y: pd.Series) -> tuple:
    """Divide os dados em conjuntos de treino e teste."""
    return train_test_split(X, y, test_size=0.1, random_state=42, stratify=y)


def impute_data(X_train: pd.DataFrame, X_test: pd.DataFrame) -> tuple:
    """Realiza a imputação dos valores ausentes."""
    imputer = SimpleImputer(strategy="mean")
    X_train_imputed = imputer.fit_transform(X_train)
    X_test_imputed = imputer.transform(X_test)
    return X_train_imputed, X_test_imputed, imputer


def train_model(X_train: pd.DataFrame, y_train: pd.Series) -> RandomForestClassifier:
    """Treina o modelo RandomForestClassifier."""
    # OrderedDict([('max_depth', 10), ('n_estimators', 215), ('random_state', 3)])
    model = RandomForestClassifier(n_estimators=215, max_depth=10, random_state=3, verbose=2)
    model.fit(X_train, y_train)
    return model
    # param_dist = {
    #     "n_estimators": Integer(16, 215),
    #     "max_depth": Integer(3,10),
    #     "random_state": Integer(0,56),
    # }

    # # Criar o objeto BayesSearchCV
    # bayes_search = BayesSearchCV(estimator=RandomForestClassifier(random_state=42),
    #                          search_spaces=param_dist,
    #                          n_iter=150,  # Número de combinações a serem testadas
    #                          cv=5,
    #                          scoring='neg_mean_squared_error',
    #                          verbose=2,
    #                          n_jobs=-1)

    # # Executar a otimização bayesiana
    # bayes_search.fit(X_train, y_train)

    # # Melhor combinação de parâmetros
    # print("Melhores parâmetros encontrados:")
    # print(bayes_search.best_params_)

    # # Avaliar o desempenho do melhor modelo
    # best_model = bayes_search.best_estimator_
    # return best_model


def evaluate_model(model: RandomForestClassifier, X_test: pd.DataFrame, y_test: pd.Series) -> None:
    """Avalia o modelo treinado e imprime os resultados."""
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print(f"Acurácia: {accuracy * 100:.2f}%")
    print("\nRelatório de Classificação:")
    print(classification_report(y_test, y_pred))


def save_model(model, imputer, encoder, scaler) -> None:
    """Salva o modelo e os pré-processadores."""
    joblib.dump(model, "modelo_random_forest_v12.pkl")
    joblib.dump(imputer, "imputer.pkl")
    joblib.dump(encoder, "encoder.pkl")
    joblib.dump(scaler, "scaler.pkl")


def main():
    # Carregar os dados
    df = load_data("bot-web/data/dados_gerais.csv")

    # Pré-processamento dos dados
    X, y, encoder, scaler = preprocess_data(df)

    # Divisão dos dados em treino e teste
    X_train, X_test, y_train, y_test = split_data(X, y)

    # Imputação dos valores ausentes
    X_train_imputed, X_test_imputed, imputer = impute_data(X_train, X_test)

    # Treinamento do modelo
    model = train_model(X_train_imputed, y_train)

    # Avaliação do modelo
    evaluate_model(model, X_test_imputed, y_test)

    # Salvar o modelo e os pré-processadores
    save_model(model, imputer, encoder, scaler)


if __name__ == "__main__":
    main()
