import pandas as pd
import joblib
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.metrics import classification_report
import tensorflow as tf


def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de um arquivo CSV e salva em formato Parquet."""
    df = pd.read_csv(file_path)
    parquet_path = file_path.replace(".csv", ".parquet")
    df.to_parquet(parquet_path, engine="pyarrow")
    return pd.read_parquet(parquet_path)


def preprocess_data(df: pd.DataFrame, encoder: LabelEncoder, scaler: StandardScaler) -> tuple:
    """Realiza o pré-processamento dos dados."""
    df["resultado_enconder"] = encoder.transform(df["rs"])
    df["resultado_scaler"] = scaler.transform(df[["resultado_enconder"]])

    X = df.drop(
        [
            "resultado_scaler",
            "resultado_enconder",
            "tl_0",
            "tv_0",
            "tl_1",
            "tv_1",
            "rs",
        ],
        axis=1,
    )
    y = df["resultado_enconder"]

    return X, y


def impute_data(X: pd.DataFrame, imputer: SimpleImputer) -> pd.DataFrame:
    """Realiza a imputação dos valores ausentes."""
    return imputer.transform(X)


def inverse_transform(
    scaler: StandardScaler, encoder: LabelEncoder, y_scaled: pd.Series
) -> pd.Series:
    """Reverte a transformação dos resultados escalados para os valores originais."""
    y_inversed_encoded = encoder.inverse_transform(y_scaled.reshape(-1, 1))
    # y_inversed = scaler.inverse_transform(y_inversed_encoded.reshape(-1, 1))
    return y_inversed_encoded


def load_and_evaluate(
    new_data_path: str,
    model_path: str,
    imputer_path: str,
    encoder_path: str,
    scaler_path: str,
) -> pd.DataFrame:
    """Carrega os novos dados, modelo e pré-processadores, e avalia o modelo."""
    # Carregar novos dados
    df_new = load_data(new_data_path)

    # Carregar modelo e pré-processadores
    model = tf.keras.models.load_model(model_path)
    imputer = joblib.load(imputer_path)
    encoder = joblib.load(encoder_path)
    scaler = joblib.load(scaler_path)

    # Pré-processar novos dados
    X_new, y_new = preprocess_data(df_new, encoder, scaler)
    # X_new_imputed = impute_data(X_new, imputer)

    # Avaliar modelo
    # num_classes = len(y_new.unique())
    # y_new_cat = tf.keras.utils.to_categorical(y_new, num_classes=num_classes)
    # _, accuracy = model.evaluate(X_new_imputed, y_new_cat, verbose=0)
    y_pred = model.predict(X_new)
    y_pred_classes = y_pred.argmax(axis=-1)

    # Reverter a transformação para os valores originais
    y_pred_rs = inverse_transform(scaler, encoder, y_pred_classes)

    # Adicionar as previsões ao DataFrame
    df_new["predicted_rs"] = y_pred_rs

    # print(f"Acurácia: {accuracy * 100:.2f}%")
    print("\nRelatório de Classificação:")
    print(classification_report(df_new["rs"], df_new["predicted_rs"]))

    return df_new


def main():
    # Caminhos dos arquivos
    # model_path = "modelo_tensorflow_v86_42.h5"
    model_path = "modelo_tensorflow_v100.h5"
    imputer_path = "imputer.pkl"
    encoder_path = "encoder.pkl"
    scaler_path = "scaler.pkl"
    new_data_path = "bot-web/out/www_sofascore_com_pt_futebol_2024_06_25.csv"

    # Carregar e avaliar com novos dados
    df_new_with_predictions = load_and_evaluate(
        new_data_path, model_path, imputer_path, encoder_path, scaler_path
    )

    # Salvar o DataFrame com as previsões
    df_new_with_predictions.to_csv("predicted_results.csv", index=False)


if __name__ == "__main__":
    main()
