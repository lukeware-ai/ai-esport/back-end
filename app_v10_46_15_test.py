import pandas as pd
import joblib
from sklearn.preprocessing import StandardScaler, LabelEncoder


def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de entrada a partir de um arquivo CSV."""
    return pd.read_csv(file_path)


def load_models_and_encoders(
    model_path: str, encoder_path: str, scaler_path: str, imputer_path: str
) -> tuple:
    """Carrega o modelo, encoder, scaler e imputer a partir dos arquivos."""
    model = joblib.load(model_path)
    encoder = joblib.load(encoder_path)
    scaler = joblib.load(scaler_path)
    imputer = joblib.load(imputer_path)
    return model, encoder, scaler, imputer


def preprocess_new_data(df: pd.DataFrame, encoder, scaler) -> pd.DataFrame:
    """Pré-processa os novos dados."""
    # Aqui você deve aplicar as transformações adequadas aos novos dados
    # Certifique-se de usar as mesmas transformações usadas durante o treinamento
    df["resultado_enconder"] = encoder.transform(df["rs"])  # Exemplo hipotético
    df["resultado_scaler"] = scaler.transform(df[["resultado_enconder"]])  # Exemplo hipotético
    return df


def prepare_features(df: pd.DataFrame) -> pd.DataFrame:
    """Remove colunas irrelevantes dos dados."""
    return df.drop(
        [
            "resultado_scaler",
            "resultado_enconder",
            "tl_0",
            "tv_0",
            "tl_1",
            "tv_1",
            "rs",
        ],
        axis=1,
    )


def impute_missing_values(X: pd.DataFrame, imputer) -> pd.DataFrame:
    """Imputa valores ausentes nos dados."""
    return imputer.transform(X)


def predict(model, X: pd.DataFrame) -> pd.Series:
    """Faz previsões usando o modelo carregado."""
    return model.predict(X)


def save_predictions(df: pd.DataFrame, predictions: pd.Series, output_path: str) -> None:
    """Salva as previsões em um arquivo CSV."""
    df["previsoes"] = predictions
    df.to_csv(output_path, index=False)


from sklearn.preprocessing import StandardScaler
import pandas as pd


def calculate_accuracy(df: pd.DataFrame, scaler: StandardScaler) -> float:
    """Calcula a acurácia das previsões."""
    # Ajuste a forma dos dados para 2D antes de aplicar inverse_transform
    previsoes = df["previsoes"].values.reshape(-1, 1)
    inverce = scaler.inverse_transform(previsoes).astype(int)
    corretas = (df["resultado_enconder"] == inverce.flatten()).sum()
    total = df["resultado_enconder"].shape[0]
    return (corretas * 100) / total


def main():
    # Caminhos dos arquivos
    data_path = "bot-web/out/www_sofascore_com_pt_futebol_2024_06_22.csv"
    model_path = "modelo_mlp_regressor.pkl"
    encoder_path = "encoder.pkl"
    scaler_path = "scaler.pkl"
    imputer_path = "imputer.pkl"
    output_path = "bot-web/out/previsoes_novos_dados.csv"

    # Carregar os dados de entrada
    df_novos_dados = load_data(data_path)

    # Carregar o modelo e os encoders
    model, encoder, scaler, imputer = load_models_and_encoders(
        model_path, encoder_path, scaler_path, imputer_path
    )

    # Pré-processar os novos dados
    df_novos_dados = preprocess_new_data(df_novos_dados, encoder, scaler)

    # Preparar as features
    X_novos = prepare_features(df_novos_dados)

    # Imputar valores ausentes
    X_novos_imputed = impute_missing_values(X_novos, imputer)

    # Fazer previsões
    y_pred_novos = predict(model, X_novos_imputed)

    # Salvar previsões
    save_predictions(df_novos_dados, y_pred_novos, output_path)

    # Calcular e exibir acurácia
    acuracia = calculate_accuracy(df_novos_dados, scaler)
    print("Previsões para os novos dados:")
    print(df_novos_dados)
    print(f"\n\nAcurácia: {acuracia:.2f}%\n\n")


if __name__ == "__main__":
    main()
    # 46.15
    # 53.85
    # 65.38
