import pandas as pd
import joblib


def load_data(file_path: str) -> pd.DataFrame:
    """Carrega os dados de entrada a partir de um arquivo CSV."""
    return pd.read_csv(file_path)


def load_models_and_encoders(
    model_path: str, encoder_path: str, scaler_path: str, imputer_path: str
) -> tuple:
    """Carrega o modelo, encoder, scaler e imputer a partir dos arquivos."""
    model = joblib.load(model_path)
    encoder = joblib.load(encoder_path)
    scaler = joblib.load(scaler_path)
    imputer = joblib.load(imputer_path)
    return model, encoder, scaler, imputer


def preprocess_new_data(df: pd.DataFrame, encoder, scaler) -> pd.DataFrame:
    """Pré-processa os novos dados."""
    df["resultado_enconder"] = encoder.transform(df["rs"])
    df["resultado_scaler"] = scaler.transform(df[["resultado_enconder"]])
    return df


def prepare_features(df: pd.DataFrame, column: str, time: str) -> pd.DataFrame:
    """Remove colunas irrelevantes dos dados."""
    df = df[df[column] == time]
    return df.drop(
        [
            "resultado_scaler",
            "resultado_enconder",
            "tl_0",
            "tv_0",
            "tl_1",
            "tv_1",
            "rs",
        ],
        axis=1,
    )


def impute_missing_values(X: pd.DataFrame, imputer) -> pd.DataFrame:
    """Imputa valores ausentes nos dados."""
    return imputer.transform(X)


def predict(model, X: pd.DataFrame) -> pd.Series:
    """Faz previsões usando o modelo carregado."""
    return model.predict(X)


def save_predictions(df: pd.DataFrame, predictions: pd.Series, output_path: str) -> None:
    """Salva as previsões em um arquivo CSV."""
    df["previsoes"] = predictions
    df.to_csv(output_path, index=False)


def calculate_accuracy(df: pd.DataFrame) -> float:
    """Calcula a acurácia das previsões."""
    corretas = (df["resultado_enconder"] == df["previsoes"]).sum()
    total = df["resultado_enconder"].shape[0]
    return (corretas * 100) / total


def main():
    # Caminhos dos arquivos
    data_path = "bot-web/out/www_sofascore_com_pt_futebol_2024_06_22.csv"
    model_path = "modelo_gradient_boosting_v9_individual.pkl"
    encoder_path = "encoder.pkl"
    scaler_path = "scaler.pkl"
    imputer_path = "imputer.pkl"
    output_path = "bot-web/out/previsoes_novos_dados.csv"

    # Carregar os dados de entrada
    df_novos_dados = load_data(data_path)

    # Carregar o modelo e os encoders
    model, encoder, scaler, imputer = load_models_and_encoders(
        model_path, encoder_path, scaler_path, imputer_path
    )

    # Pré-processar os novos dados
    df_novos_dados = preprocess_new_data(df_novos_dados, encoder, scaler)

    # Preparar as features
    X_novos = prepare_features(df_novos_dados, "tl_0", "vasco")

    # Imputar valores ausentes
    X_novos_imputed = impute_missing_values(X_novos, imputer)

    # Fazer previsões
    y_pred_novos = predict(model, X_novos_imputed)

    # Salvar previsões
    save_predictions(X_novos, y_pred_novos, output_path)

    X_novos["resultado_enconder"] = df_novos_dados["resultado_enconder"]
    X_novos["rs"] = df_novos_dados["rs"]

    # Calcular e exibir acurácia
    acuracia = calculate_accuracy(X_novos)
    print("Previsões para os novos dados:")
    print(X_novos)
    print(f"\n\nAcurácia: {acuracia:.2f}%\n\n")


if __name__ == "__main__":
    main()
