O parâmetro `activation` da camada `Dense` no Keras especifica a função de ativação usada pelos neurônios dessa camada. Aqui estão as opções mais comuns que você pode usar:

1. **`'relu'`** (Rectified Linear Unit):
   - Retorna `max(0, x)`.
   - Muito popular devido à sua simplicidade e eficiência em evitar problemas de gradientes desvanecentes.

2. **`'sigmoid'`**:
   - Retorna uma saída no intervalo (0, 1) com a fórmula `1 / (1 + exp(-x))`.
   - Utilizada principalmente em problemas de classificação binária.

3. **`'softmax'`**:
   - Converte logits em probabilidades que somam 1.
   - Comumente usada na camada de saída para classificação multi-classe.

4. **`'tanh'`** (Tangente Hiperbólica):
   - Retorna uma saída no intervalo (-1, 1) com a fórmula `(exp(x) - exp(-x)) / (exp(x) + exp(-x))`.
   - Utilizada como alternativa à função sigmoid, mas centrada em zero.

5. **`'linear'`**:
   - Função de ativação linear, retorna a entrada tal como está.
   - Útil para problemas de regressão onde a saída pode ser qualquer valor real.

6. **`'elu'`** (Exponential Linear Unit):
   - Retorna `x` se `x > 0`, caso contrário `α * (exp(x) - 1)`.
   - Funciona como ReLU, mas suaviza a saída para valores negativos.

7. **`'selu'`** (Scaled Exponential Linear Unit):
   - Como `elu`, mas ajusta a saída para garantir a auto-normalização.
   - `selu` pode ser útil para redes profundas.

8. **`'softplus'`**:
   - Retorna `log(1 + exp(x))`.
   - Uma versão suavizada de ReLU.

9. **`'softsign'`**:
   - Retorna `x / (1 + abs(x))`.
   - Outra função suavizada.

10. **`'swish'`**:
    - Retorna `x * sigmoid(x)`.
    - Introduzida pelo Google, combina as vantagens de `relu` e `sigmoid`.

11. **`'hard_sigmoid'`**:
    - Uma aproximação eficiente de sigmoid, retorna `min(max(0, x * 0.2 + 0.5), 1)`.

### Exemplos de Uso

Aqui estão alguns exemplos de como usar essas funções de ativação:

```python
from tensorflow.keras.layers import Dense

# ReLU activation
layer = Dense(64, activation='relu')

# Sigmoid activation
layer = Dense(1, activation='sigmoid')

# Softmax activation
layer = Dense(3, activation='softmax')

# Tanh activation
layer = Dense(64, activation='tanh')

# Linear activation (default if not specified)
layer = Dense(64, activation='linear')

# ELU activation
layer = Dense(64, activation='elu')

# SELU activation
layer = Dense(64, activation='selu')

# Softplus activation
layer = Dense(64, activation='softplus')

# Softsign activation
layer = Dense(64, activation='softsign')

# Swish activation
layer = Dense(64, activation='swish')

# Hard sigmoid activation
layer = Dense(64, activation='hard_sigmoid')
```

Cada função de ativação tem suas características e pode ser mais adequada para certos tipos de problemas. A escolha correta pode afetar significativamente o desempenho e a convergência do modelo.