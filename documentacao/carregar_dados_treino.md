A melhor opção atualmente para reduzir o consumo de CPU e memória ao carregar dados de treinamento é usar o formato **Parquet**. Ele oferece uma excelente combinação de velocidade e compressão eficiente. Além disso, usar a biblioteca **Dask** pode ajudar a manipular dados maiores que a memória disponível, dividindo o processamento em partes menores.

### Resumo da Melhor Abordagem

1. **Formato de Arquivo**: Utilize o formato **Parquet**.
   ```python
   import pandas as pd

   # Carregar dados em formato Parquet
   df = pd.read_parquet('data.parquet')
   ```

2. **Manipulação de Grandes Volumes de Dados**: Use **Dask** para processar dados maiores que a memória.
   ```python
   import dask.dataframe as dd

   # Carregar dados em formato Parquet com Dask
   ddf = dd.read_parquet('data.parquet')
   ```

3. **Conversão de Tipos de Dados**: Defina explicitamente os tipos de dados ao carregar os dados para economizar memória.
   ```python
   dtype = {
       'coluna1': 'int32',
       'coluna2': 'float32',
   }
   df = pd.read_parquet('data.parquet').astype(dtype)
   ```

4. **Liberar Memória**: Libere objetos não utilizados e colete o lixo.
   ```python
   import gc
   del df
   gc.collect()
   ```

### Exemplo Completo

```python
import pandas as pd
import dask.dataframe as dd
import gc

# Definindo tipos de dados para economizar memória
dtype = {
    'coluna1': 'int32',
    'coluna2': 'float32',
}

# Carregar dados em formato Parquet com Dask
ddf = dd.read_parquet('data.parquet')

# Converter tipos de dados
ddf = ddf.astype(dtype)

# Realizar operações necessárias no DataFrame ddf
# ...

# Liberar memória não utilizada
del ddf
gc.collect()
```

### Benefícios
- **Parquet**: Rápido para leitura/escrita e eficiente em termos de espaço.
- **Dask**: Permite trabalhar com dados maiores que a memória, distribuindo a carga de processamento.
- **Definição de Tipos de Dados**: Reduz o uso de memória ao carregar os dados.

Essa abordagem otimiza o uso de CPU e memória, facilitando o processamento de grandes volumes de dados de treinamento para sua AI.