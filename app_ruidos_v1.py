import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler, LabelEncoder

# Carregar os dados
df = pd.read_csv("bot-web/data/dados_gerais.csv")

# Pré-processamento dos dados
encoder = LabelEncoder()
scaler = StandardScaler()

# Codificação da variável 'rs' e padronização
df["resultado_enconder"] = encoder.fit_transform(df["rs"])
df["resultado_scaler"] = scaler.fit_transform(df[["resultado_enconder"]])

# Remover colunas irrelevantes para análise
colunas_para_remover = [
    "resultado_scaler",
    "resultado_enconder",
    "tl_0",
    "tv_0",
    "tl_2",
    "tv_1",
    "rs",
]
X = df.drop(colunas_para_remover, axis=1)

# Função para capping de outliers


def cap_outliers(df, column, lower_quantile=0.05, upper_quantile=0.95):
    lower_bound = df[column].quantile(lower_quantile)
    upper_bound = df[column].quantile(upper_quantile)
    df[column] = np.where(df[column] < lower_bound, lower_bound, df[column])
    df[column] = np.where(df[column] > upper_bound, upper_bound, df[column])
    return df


# Aplicar capping em todas as colunas numéricas
for col in X.select_dtypes(include=np.number).columns:
    X = cap_outliers(X, col)

# Estatísticas descritivas
print(X.describe())

# Visualizações gráficas

# Box Plot
plt.figure(figsize=(20, 10))
sns.boxplot(data=X)
plt.xticks(rotation=90)
plt.title("Box Plot das Variáveis")
plt.show()

# Histogramas
plt.figure(figsize=(20, 10))
X.hist(bins=30, figsize=(20, 10))
plt.suptitle("Histograma das Variáveis")
plt.show()

# Scatter Plot
plt.figure(figsize=(10, 6))
sns.scatterplot(x=X.iloc[:, 0], y=X.iloc[:, 1])
plt.title("Scatter Plot entre a Primeira e a Segunda Variável")
plt.xlabel(X.columns[0])
plt.ylabel(X.columns[1])
plt.show()

# Heatmap de correlação
plt.figure(figsize=(20, 10))
sns.heatmap(X.corr(), annot=True, cmap="coolwarm")
plt.title("Heatmap de Correlação")
plt.show()

# Pair Plot
sns.pairplot(X)
plt.suptitle("Pair Plot das Variáveis", y=1.02)
plt.show()

# PCA
X_scaled = StandardScaler().fit_transform(X)
pca = PCA(n_components=2)
X_pca = pca.fit_transform(X_scaled)

plt.figure(figsize=(15, 10))
plt.scatter(X_pca[:, 0], X_pca[:, 1], alpha=0.5)
plt.title("PCA - Primeiras 2 Componentes Principais")
plt.xlabel("Componente Principal 1")
plt.ylabel("Componente Principal 2")
plt.show()
