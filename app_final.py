import pandas as pd

tl = "sao_paulo"
tv = "corinthians"

df = pd.read_csv("bot-web/data/dados_gerais.csv")

tl_cols = [col for col in df.columns if col.startswith("tl")]
df_tl = df[tl_cols]

tv_cols = [col for col in df.columns if col.startswith("tv")]
df_tv = df[tv_cols]

df_tl = df_tl[df_tl["tl_0"] == tl]
df_tv = df_tv[df_tv["tv_0"] == tv]

X = df_tl.drop(columns=["tl_0"])
y = df_tl["tl_0"]

print(X.mean().to_frame().T)
